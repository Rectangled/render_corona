import bpy

#---------------------------------------
# Particle settings UI
#---------------------------------------
class CoronaPsysPanel( bpy.types.Panel):
    bl_label = "Corona Hair Rendering"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "particle"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        psys = context.particle_system
        return renderer == 'CORONA' and context.object is not None and psys and psys.settings.type == 'HAIR'

    def draw_header( self, context):
        pass
        
    def draw( self, context):
        layout = self.layout
        crn_psys = context.particle_system.settings.corona

        layout.prop( crn_psys, "shape")
        layout.prop( crn_psys, "resolution")
        layout.label( "Thickness:")
        row = layout.row()
        row.prop( crn_psys, "root_size")
        row.prop( crn_psys, "tip_size")
        layout.prop( crn_psys, "scaling")

def register():
    bpy.utils.register_class( CoronaPsysPanel)
    
def unregister():
    bpy.utils.unregister_class( CoronaPsysPanel)
