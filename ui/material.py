import bpy

def node_tree_selector_draw(layout, mat, output_types):
	try:
		layout.prop_search( mat.corona, "node_tree", bpy.data, "node_groups")
	except:
		return False
	
	node = find_node(mat, output_types)
	if not node:
		if mat.corona.node_tree == '':
			layout.operator('corona.add_material_nodetree', icon='NODETREE')
			return False
	return True
    
def find_node(material, nodetypes):
	if not (material and material.corona and material.corona.node_tree):
		return None
		
	node_tree =  material.corona.node_tree
	
	if node_tree == '':
		return None
	
	ntree = bpy.data.node_groups[node_tree]
	
	for node in ntree.nodes:
		nt = getattr(node, "bl_idname", None)
		if nt in nodetypes:
			return node
	return None
	
#---------------------------------------
# Material preview UI
#---------------------------------------
class CoronaMaterialPreview( bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    
    bl_context = "material"
    bl_label = "Preview"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        return context.scene.render.engine in cls.COMPAT_ENGINES and context.object is not None and context.object.active_material is not None

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        
        row = layout.row()
        row.template_preview( context.material, show_buttons=False)
        layout.prop( crn_mat, "preview_quality")


#---------------------------------------
# Material settings UI
#---------------------------------------
class CoronaMaterialShading( bpy.types.Panel):
    bl_label = 'Corona Surface Shading'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None


    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        layout.label( "Blender To Corona Material Conversion:")
        row = layout.row( align = True)
        row.operator( "corona.mat_convert", icon = 'MATERIAL', text = "Convert Material")
        row.operator( "corona.mat_convert", icon = 'MATERIAL', text = "Convert All").scene_wide = True

        layout.separator()
        layout.separator()

        node_tree_selector_draw( layout, material, { 'CoronaMtlNode', 'CoronaLightMtlNode', 'CoronaVolumeMtlNode'})
        if crn_mat.node_tree != '':
            node_tree = bpy.data.node_groups[ crn_mat.node_tree]
            layout.prop_search( crn_mat, "node_output", node_tree, "nodes")

        if crn_mat.node_tree == '':
            layout.prop( crn_mat, "mtl_type")

            layout.separator()

            if crn_mat.mtl_type == 'coronamtl':
                # Diffuse settings.
                # Kd
                layout.label( "Diffuse:")
                box = layout.box()

                split = box.split( percentage = 0.7)
                col = split.column()
                col.prop( crn_mat, "diffuse_level", text = "Diffuse Level")

                split = split.split( percentage = 0.5)
                col = split.column()
                col.prop( crn_mat, "kd", text = "")
                
                col = split.column()
                col.prop( crn_mat, "use_map_kd", text = "T", toggle = True)
                if crn_mat.use_map_kd:
                    box.prop( crn_mat.map_kd, "show_settings")
                    if crn_mat.map_kd.show_settings:
                        box.prop_search( crn_mat.map_kd, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
#                        box.template_image( crn_mat.map_kd,
#                                         "texture", 
#                                         crn_mat.image_user)
                        box.prop( crn_mat.map_kd, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_kd, "uOffset")
                        row.prop( crn_mat.map_kd, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_kd, "uScaling")
                        row.prop( crn_mat.map_kd, "vScaling")

                # Translucency
                split = box.split( percentage = 0.85)
                col = split.column()
                
                col.prop( crn_mat, "translucency_level", text = "Translucency Level")
                if crn_mat.use_map_translucency_level:
                    box.prop( crn_mat.map_translucency_level, "show_settings")
                    if crn_mat.map_translucency_level.show_settings:
                        box.prop_search( crn_mat.map_translucency_level, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_translucency_level, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_translucency_level, "uOffset")
                        row.prop( crn_mat.map_translucency_level, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_translucency_level, "uScaling")
                        row.prop( crn_mat.map_translucency_level, "vScaling")

                col = split.column()
                col.prop( crn_mat, "use_map_translucency_level", text = "T", toggle = True)
                
                split = box.split( percentage = 0.85)
                col = split.column()
                
                col.prop( crn_mat, "translucency", text = "")
                if crn_mat.use_map_translucency:
                    box.prop( crn_mat.map_translucency, "show_settings")
                    if crn_mat.map_translucency.show_settings:
                        box.prop_search( crn_mat.map_translucency, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_translucency, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_translucency, "uOffset")
                        row.prop( crn_mat.map_translucency, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_translucency, "uScaling")
                        row.prop( crn_mat.map_translucency, "vScaling")

                col = split.column()
                col.prop( crn_mat, "use_map_translucency", text = "T", toggle = True)
                

                layout.separator()
                
                # Reflection settings.
                layout.label( "Reflection:")
                box = layout.box()
                # Ns
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "ns", text = "Reflection Level")
                
                col = split.column()
                col.prop( crn_mat, "use_map_ns", text = "T", toggle = True)
                if crn_mat.use_map_ns:
                    box.prop( crn_mat.map_ns, "show_settings")
                    if crn_mat.map_ns.show_settings:
                        box.prop_search( crn_mat.map_ns, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_ns, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_ns, "uOffset")
                        row.prop( crn_mat.map_ns, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_ns, "uScaling")
                        row.prop( crn_mat.map_ns, "vScaling")

                # Ks
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "ks", text = "")
                
                col = split.column()
                col.prop( crn_mat, "use_map_ks", text = "T", toggle = True)
                if crn_mat.use_map_ks:
                    box.prop( crn_mat.map_ks, "show_settings")
                    if crn_mat.map_ks.show_settings:
                        box.prop_search( crn_mat.map_ks, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_ks, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_ks, "uOffset")
                        row.prop( crn_mat.map_ks, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_ks, "uScaling")
                        row.prop( crn_mat.map_ks, "vScaling")
                    
                # Reflect glossiness
                row = box.row( align = True)
                row.prop( crn_mat, "reflect_glossiness")
                # Reflect fresnel
                row.prop( crn_mat, "reflect_fresnel")
                # Anisotropy
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "anisotropy", text = "Anisotropy")
                
                col = split.column()
                col.prop( crn_mat, "use_map_aniso", text = "T", toggle = True)
                if crn_mat.use_map_aniso:
                    box.prop( crn_mat.map_aniso, "show_settings")
                    if crn_mat.map_aniso.show_settings:
                        box.prop_search( crn_mat.map_aniso, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_aniso, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_aniso, "uOffset")
                        row.prop( crn_mat.map_aniso, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_aniso, "uScaling")
                        row.prop( crn_mat.map_aniso, "vScaling")
                # Anisotropy rotation.
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "aniso_rotation", text = "Anisotropy Rotation")
                
                col = split.column()
                col.prop( crn_mat, "use_map_aniso_rot", text = "T", toggle = True)
                if crn_mat.use_map_aniso_rot:
                    box.prop( crn_mat.map_aniso_rot, "show_settings")
                    if crn_mat.map_aniso_rot.show_settings:
                        box.prop_search( crn_mat.map_aniso_rot, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_aniso_rot, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_aniso_rot, "uOffset")
                        row.prop( crn_mat.map_aniso_rot, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_aniso_rot, "uScaling")
                        row.prop( crn_mat.map_aniso_rot, "vScaling")

                layout.separator()

                # Refraction settings.   
                layout.label( "Refraction:")     
                box = layout.box()
                split = box.split( percentage = 0.7)
                col = split.column()
                col.prop( crn_mat, "refract_level", text = "Refraction Level")
                
                split = split.split( percentage = 0.5)
                col = split.column()
                col.prop( crn_mat, "refract", text = "")
               
                col = split.column()
                col.prop( crn_mat, "use_map_refract", text = "T", toggle = True)
                if crn_mat.use_map_refract:
                    box.prop( crn_mat.map_refract, "show_settings")
                    if crn_mat.map_refract.show_settings:
                        box.prop_search( crn_mat.map_refract, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_refract, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_refract, "uOffset")
                        row.prop( crn_mat.map_refract, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_refract, "uScaling")
                        row.prop( crn_mat.map_refract, "vScaling")
                
                # Refraction Gloss / IOR
                row = box.row( align = True)
                row.prop( crn_mat, "refract_glossiness")
                row.prop( crn_mat, "ni")

                split = box.split()
                col = split.column()
                col.active = crn_mat.refract_thin == False
                col.prop(  crn_mat, "refract_caustics")

                col = split.column()
                col.active = crn_mat.refract_caustics == False
                col.prop( crn_mat, "refract_thin")
                
                layout.separator()

                
                # Volumetric / Glass Absorption
                layout.label( "Volumetric Absorption / Scattering")
                box = layout.box()
                box.label( "Absorption:")

                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "absorption_color", text = "")

                col = split.column()
                col.prop( crn_mat, "use_map_absorption", text = "T", toggle = True)
                if crn_mat.use_map_absorption:
                    box.prop( crn_mat.map_absorption, "show_settings")
                    if crn_mat.map_absorption.show_settings:
                        box.prop_search( crn_mat.map_absorption, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_absorption, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_absorption, "uOffset")
                        row.prop( crn_mat.map_absorption, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_absorption, "uScaling")
                        row.prop( crn_mat.map_absorption, "vScaling")
                          
                box.prop( crn_mat, "absorption_distance", text = "Absorption Distance")

                box.label( "Scattering:")
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "scattering_albedo", text = "")

                col = split.column()
                col.prop( crn_mat, "use_map_scattering", text = "T", toggle = True)
                if crn_mat.use_map_scattering:
                    box.prop( crn_mat.map_scattering, "show_settings")
                    if crn_mat.map_scattering.show_settings:
                        box.prop_search( crn_mat.map_scattering,
                                        "texture",
                                        material,
                                        "texture_slots",
                                        text = "Texture")
                        box.prop( crn_mat.map_scattering, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_scattering, "uOffset")
                        row.prop( crn_mat.map_scattering, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_scattering, "uScaling")
                        row.prop( crn_mat.map_scattering, "vScaling")

                box.prop( crn_mat, "mean_cosine")
                
                # Opacity
                layout.label( "Opacity:")
                box = layout.box()
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "opacity", text = "")
                
                col = split.column()
                col.prop( crn_mat, "use_map_opacity", text = "T", toggle = True)
                if crn_mat.use_map_opacity:
                    box.prop( crn_mat.map_opacity, "show_settings")
                    if crn_mat.map_opacity.show_settings:
                        box.prop_search( crn_mat.map_opacity, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_opacity, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_opacity, "uOffset")
                        row.prop( crn_mat.map_opacity, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_opacity, "uScaling")
                        row.prop( crn_mat.map_opacity, "vScaling")
                layout.separator()
                
                # Normal
                layout.label( "Normal:")
                box = layout.box()
                box.prop( crn_mat, "use_map_normal", text = "Use Normal Texture", toggle = True)
                if crn_mat.use_map_normal:
                    box.prop( crn_mat.map_normal, "show_settings")
                    if crn_mat.map_normal.show_settings:
                        box.prop_search( crn_mat.map_normal, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_normal, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_normal, "uOffset")
                        row.prop( crn_mat.map_normal, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_normal, "uScaling")
                        row.prop( crn_mat.map_normal, "vScaling")

#                box.prop( crn_mat, "use_map_bump", text = "Use Bump Texture", toggle = True)
#                if crn_mat.use_map_bump:
#                    box.prop( crn_mat.map_bump, "show_settings")
#                    if crn_mat.map_bump.show_settings:
#                        box.prop_search( crn_mat.map_bump, 
#                                        "texture", 
#                                        material, 
#                                        "texture_slots", 
#                                        text = "Texture")
#                        box.prop( crn_mat.map_bump, "intensity")
#                        row = box.row( align = True)
#                        row.prop( crn_mat.map_bump, "uOffset")
#                        row.prop( crn_mat.map_bump, "vOffset")
#                        row = box.row( align = True)
#                        row.prop( crn_mat.map_bump, "uScaling")
#                        row.prop( crn_mat.map_bump, "vScaling")
                
            if crn_mat.mtl_type == 'coronalightmtl' or crn_mat.mtl_type == 'coronavolumemtl':
                # Emission settings
                layout.label( "Emission:")
                box = layout.box()
                split = box.split( percentage = 0.7)
                col = split.column()
                col.prop( crn_mat, "emission_mult", text = "Emission Level")
                
                split = split.split( percentage = 0.5)
                col = split.column()
                col.prop( crn_mat, "ke", text = "")
                
                col = split.column()
                col.prop( crn_mat, "use_map_ke", text = "T", toggle = True)
                if crn_mat.use_map_ke:
                    box.prop( crn_mat.map_ke, "show_settings")
                    if crn_mat.map_ke.show_settings:
                        box.prop_search( crn_mat.map_ke, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_ke, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_ke, "uOffset")
                        row.prop( crn_mat.map_ke, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_ke, "uScaling")
                        row.prop( crn_mat.map_ke, "vScaling")

                box.separator()
                
                box.prop( crn_mat, "emission_gloss", text = "Directionality")

                layout.separator()

            if crn_mat.mtl_type == 'coronalightmtl':
                # IES settings.
                layout.prop( crn_mat, "use_ies")
                box = layout.box()
                box.active = crn_mat.use_ies
                box.prop( crn_mat, "ies_profile")
                box.prop( crn_mat, "keep_sharp", text = "Keep Profile Sharp")
                
                layout.separator()
                
                # Opacity
                layout.label( "Opacity:")
                box = layout.box()
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "opacity", text = "")
                
                col = split.column()
                col.prop( crn_mat, "use_map_opacity", text = "T", toggle = True)
                if crn_mat.use_map_opacity:
                    box.prop( crn_mat.map_opacity, "show_settings")
                    if crn_mat.map_opacity.show_settings:
                        box.prop_search( crn_mat.map_opacity, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_opacity, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_opacity, "uOffset")
                        row.prop( crn_mat.map_opacity, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_opacity, "uScaling")
                        row.prop( crn_mat.map_opacity, "vScaling")


            if crn_mat.mtl_type == 'coronaportalmtl':
                layout.label( "NOTE: Portal materials work differently in Corona")
                layout.label( "than in other renderers. You don't have to cover")
                layout.label( "all openings, and they work with scenes that have")
                layout.label( "both interior and exterior.")
                layout.label( "There is no poly count restriction, and there is")
                layout.label( "also a portal switch in regular CoronaMtl to turn")
                layout.label( " any material (e.g. glass window material) into a portal.")

            if crn_mat.mtl_type == 'coronavolumemtl':
                # Volumetric / Glass Absorption
                layout.label( "Volumetric Absorption / Scattering")
                box = layout.box()
                box.label( "Absorption:")

                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "absorption_color", text = "")

                col = split.column()
                col.prop( crn_mat, "use_map_absorption", text = "T", toggle = True)
                if crn_mat.use_map_absorption:
                    box.prop( crn_mat.map_absorption, "show_settings")
                    if crn_mat.map_absorption.show_settings:
                        box.prop_search( crn_mat.map_absorption, 
                                        "texture", 
                                        material, 
                                        "texture_slots", 
                                        text = "Texture")
                        box.prop( crn_mat.map_absorption, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_absorption, "uOffset")
                        row.prop( crn_mat.map_absorption, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_absorption, "uScaling")
                        row.prop( crn_mat.map_absorption, "vScaling")
                          
                box.prop( crn_mat, "absorption_distance", text = "Absorption Distance")

                box.label( "Scattering:")
                split = box.split( percentage = 0.85)
                col = split.column()
                col.prop( crn_mat, "scattering_albedo", text = "")

                col = split.column()
                col.prop( crn_mat, "use_map_scattering", text = "T", toggle = True)
                if crn_mat.use_map_scattering:
                    box.prop( crn_mat.map_scattering, "show_settings")
                    if crn_mat.map_scattering.show_settings:
                        box.prop_search( crn_mat.map_scattering,
                                        "texture",
                                        material,
                                        "texture_slots",
                                        text = "Texture")
                        box.prop( crn_mat.map_scattering, "intensity")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_scattering, "uOffset")
                        row.prop( crn_mat.map_scattering, "vOffset")
                        row = box.row( align = True)
                        row.prop( crn_mat.map_scattering, "uScaling")
                        row.prop( crn_mat.map_scattering, "vScaling")

                box.prop( crn_mat, "mean_cosine")

            if crn_mat.mtl_type == 'coronamtl':
                layout.separator()
                layout.prop( crn_mat, "rounded_corners")
                layout.separator()
                layout.label( "Light Portal Material", icon = "OUTLINER_OB_LAMP")
                layout.prop( crn_mat, "as_portal")
                
            if crn_mat.mtl_type != 'coronaportalmtl':
                layout.separator() 
                
                # Ray visibility / non-shaded
                layout.label("Ray Invisibility", icon = "RESTRICT_VIEW_OFF")
                
                box = layout.box()
                row = box.row()
                row.alignment = 'CENTER'
                row.prop(crn_mat, "ray_gi_inv")
                row.prop(crn_mat, "ray_direct_inv")
                row.prop(crn_mat, "ray_refract_inv")
                row = box.row()
                row.alignment = 'CENTER'
                row.prop(crn_mat, "ray_reflect_inv")
                row.prop(crn_mat, "ray_shadows_inv")
                
#---------------------------------------
# Ubershader operator UI
#---------------------------------------
class CoronaUbershaderPanel( bpy.types.Panel):
    bl_label = "Corona Material Nodes"
    bl_space_type = "NODE_EDITOR"
    bl_region_type = 'UI'
    
    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer in ['CORONA', 'CYCLES'] and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None
    
    def draw( self, context):
        layout = self.layout
        layout.operator( "corona.create_ubershader")


def register():
    bpy.utils.register_class( CoronaUbershaderPanel)
    bpy.utils.register_class( CoronaMaterialPreview)
    bpy.utils.register_class( CoronaMaterialShading)
def unregister():
    bpy.utils.unregister_class( CoronaUbershaderPanel)
    bpy.utils.unregister_class( CoronaMaterialShading)
    bpy.utils.unregister_class( CoronaMaterialPreview)

        

