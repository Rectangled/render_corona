import bpy

#---------------------------------------
# Sun UI
#---------------------------------------
class CoronaSunPanel(bpy.types.Panel):
    bl_label = "Corona Sun"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    COMPAT_ENGINES = {'CORONA'}
    bl_context = "world"

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA'

    def draw_header( self, context):
        crn_sky_props = context.scene.corona_sky
        header = self.layout

        header.scale_y = 1.0
        header.prop( crn_sky_props, "use_sun")

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_sky_props = scene.corona_sky

        layout.active = crn_sky_props.use_sun

        split = layout.split()
        col = split.column()
        col.label( "Sun to export:")
        col.prop( crn_sky_props, "sun_lamp", text = '')
        col.label( "Sun Color:")
        col.prop( crn_sky_props, "sun_color", text = "")
        
        col = split.column()
        col.label( "Size Multiplier:")
        col.prop( crn_sky_props, "sun_size_mult", text = "")
        col.label( "Energy Multiplier:")
        col.prop( crn_sky_props, "sun_energy_mult")

#---------------------------------------
# Environment UI
#---------------------------------------
class CoronaWorldPanel(bpy.types.Panel):
    bl_label = "Corona Environment"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "world"
    COMPAT_ENGINES = {'CORONA'}
    
    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA'
    
    def draw( self, context):
        layout = self.layout
        scene = context.scene
        world = scene.world
        crn_world = world.corona

        layout.prop( crn_world, "mode")
        if crn_world.mode == 'color':
            layout.prop( crn_world, "enviro_color", text = "")
            
        if crn_world.mode == 'latlong':    
            layout.prop( crn_world, "enviro_tex")
            layout.prop( crn_world, "latlong_intensity")
            
            row = layout.row( align = True)
            row.prop( crn_world, "latlong_uOffset")
            row.prop( crn_world, "latlong_vOffset")

            row = layout.row( align = True)
            row.prop( crn_world, "latlong_uScaling")
            row.prop( crn_world, "latlong_vScaling")
            
        if crn_world.mode == 'sky':
            row = layout.row()
            row.prop( crn_world, "sky_intensity")
            row.prop( crn_world, "sky_turbidity")

        if crn_world.mode == 'rayswitch':
            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "gi_color")
            col = split.column()
            col.prop( crn_world, "gi_use_tex", text = "T", toggle = True)
            if crn_world.gi_use_tex:
                layout.prop( crn_world.map_gi, "show_settings")
                if crn_world.map_gi.show_settings:
                    layout.prop( crn_world.map_gi, "texture")
                    layout.prop( crn_world.map_gi, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_gi, "uOffset")
                    row.prop( crn_world.map_gi, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_gi, "uScaling")
                    row.prop( crn_world.map_gi, "vScaling")
            
            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "reflect_color")
            col = split.column()
            col.prop( crn_world, "reflect_use_tex", toggle = True, text = "T")
            if crn_world.reflect_use_tex:
                layout.prop( crn_world.map_reflect, "show_settings")
                if crn_world.map_reflect.show_settings:
                    layout.prop( crn_world.map_reflect, "texture")
                    layout.prop( crn_world.map_reflect, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_reflect, "uOffset")
                    row.prop( crn_world.map_reflect, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_reflect, "uScaling")
                    row.prop( crn_world.map_reflect, "vScaling")

            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "refract_color")
            col = split.column()
            col.prop( crn_world, "refract_use_tex", toggle = True, text = "T")
            if crn_world.refract_use_tex:
                layout.prop( crn_world.map_refract, "show_settings")
                if crn_world.map_refract.show_settings:
                    layout.prop( crn_world.map_refract, "texture")
                    layout.prop( crn_world.map_refract, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_refract, "uOffset")
                    row.prop( crn_world.map_refract, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_refract, "uScaling")
                    row.prop( crn_world.map_refract, "vScaling")

            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "direct_color")
            col = split.column()
            col.prop( crn_world, "direct_use_tex", toggle = True, text = "T")
            if crn_world.direct_use_tex:
                layout.prop( crn_world.map_direct, "show_settings")
                if crn_world.map_direct.show_settings:
                    layout.prop( crn_world.map_direct, "texture")
                    layout.prop( crn_world.map_direct, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_direct, "uOffset")
                    row.prop( crn_world.map_direct, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_direct, "uScaling")
                    row.prop( crn_world.map_direct, "vScaling")

        layout.separator()
        
        # Volumetric / Glass Absorption
        layout.prop( crn_world, "use_global_medium")
        if crn_world.use_global_medium:
            box = layout.box()
            box.label( "Absorption:")

            split = box.split( percentage = 0.85)
            col = split.column()
            col.prop( crn_world, "absorption_color", text = "")

            col = split.column()
            col.prop( crn_world, "use_map_absorption", text = "T", toggle = True)
            if crn_world.use_map_absorption:
                box.prop( crn_world.map_absorption, "show_settings")
                if crn_world.map_absorption.show_settings:
                    box.prop_search( crn_world.map_absorption, 
                                    "texture", 
                                    world, 
                                    "texture_slots", 
                                    text = "Texture")
                    box.prop( crn_world.map_absorption, "intensity")
                    row = box.row( align = True)
                    row.prop( crn_world.map_absorption, "uOffset")
                    row.prop( crn_world.map_absorption, "vOffset")
                    row = box.row( align = True)
                    row.prop( crn_world.map_absorption, "uScaling")
                    row.prop( crn_world.map_absorption, "vScaling")
                      
            box.prop( crn_world, "absorption_distance", text = "Absorption Distance")

            box.label( "Scattering:")
            split = box.split( percentage = 0.85)
            col = split.column()
            col.prop( crn_world, "scattering_albedo", text = "")

            col = split.column()
            col.prop( crn_world, "use_map_scattering", text = "T", toggle = True)
            if crn_world.use_map_scattering:
                box.prop( crn_world.map_scattering, "show_settings")
                if crn_world.map_scattering.show_settings:
                    box.prop_search( crn_world.map_scattering,
                                    "texture",
                                    world,
                                    "texture_slots",
                                    text = "Texture")
                    box.prop( crn_world.map_scattering, "intensity")
                    row = box.row( align = True)
                    row.prop( crn_world.map_scattering, "uOffset")
                    row.prop( crn_world.map_scattering, "vOffset")
                    row = box.row( align = True)
                    row.prop( crn_world.map_scattering, "uScaling")
                    row.prop( crn_world.map_scattering, "vScaling")

            box.prop( crn_world, "mean_cosine")
        
def register():
    bpy.utils.register_class( CoronaWorldPanel)
    bpy.utils.register_class( CoronaSunPanel)
def unregister():        
    bpy.utils.unregister_class( CoronaWorldPanel)
    bpy.utils.unregister_class( CoronaSunPanel)
