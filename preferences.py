import bpy
from bpy.props import StringProperty

#------------------------------------
# User preferences UI
#------------------------------------
class CoronaPreferencesPanel( bpy.types.AddonPreferences):
    bl_idname = __package__
    corona_path = StringProperty( name = "Corona Path",
                                       description = "Path to Corona executable directory",
                                       subtype = 'DIR_PATH',
                                       default = "")

    def draw(self, context):
        self.layout.prop( self, "corona_path")
    
        
def register():
    bpy.utils.register_class( CoronaPreferencesPanel)
def unregister():      
    bpy.utils.unregister_class( CoronaPreferencesPanel)  
