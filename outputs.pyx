import bpy
import math, mathutils
from .util  import *

#------------------------------------
# Material library writing function.
#------------------------------------
def write_mtl( mat_file, default_mat, mat_data, node):
    '''
    Write the material to the open mtl file
    mat_data is the variable for bpy.data.materials[material name]
    '''        
    crn_mat = mat_data.corona
    fw = mat_file.write
    if default_mat:
        fw( 'Kd 0.8, 0.8, 0.8\n')
        return
    if node is None:
        write_list = []
        if crn_mat.mtl_type == 'coronaportalmtl':
            fw('#CORONA Portal\n')
            fw('Kd 0 0 0\n')
            fw('#CORONA Opacity 0 0 0\n')
            return 
            
        elif crn_mat.mtl_type == 'coronamtl':
            if crn_mat.as_portal:
                fw( '#CORONA Portal\n')
            # Diffuse
            diffuseMult = crn_mat.diffuse_level
            diffuse_col = [crn_mat.kd[0] * diffuseMult,
                           crn_mat.kd[1] * diffuseMult,
                           crn_mat.kd[2] * diffuseMult]
            fw('Kd %.2f %.2f %.2f\n' % (diffuse_col[0], 
                                        diffuse_col[1], 
                                        diffuse_col[2])) 
            if crn_mat.use_map_kd and crn_mat.map_kd.texture != '':
                if is_uv_img( crn_mat.map_kd.texture):
                    texture = get_tex_path(crn_mat.map_kd.texture)
                    fw('map_Kd ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_kd.intensity, 
                                                                            crn_mat.map_kd.uOffset,
                                                                            crn_mat.map_kd.vOffset,
                                                                            crn_mat.map_kd.uScaling,
                                                                            crn_mat.map_kd.vScaling,
                                                                            texture))

            # Translucency
            if crn_mat.translucency[:] > (0.0, 0.0, 0.0):
                fw('#CORONA Translucency %.2f %.2f %.2f\n' % (crn_mat.translucency[0], 
                                                              crn_mat.translucency[1], 
                                                              crn_mat.translucency[2]))
                                                              
            if crn_mat.use_map_translucency and crn_mat.map_translucency.texture != '':
                if is_uv_img( crn_mat.map_translucency.texture):
                    texture = get_tex_path(crn_mat.map_translucency.texture)
                    fw('map_translucency ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_translucency.intensity, 
                                                                        crn_mat.map_translucency.uOffset,
                                                                        crn_mat.map_translucency.vOffset,
                                                                        crn_mat.map_translucency.uScaling,
                                                                        crn_mat.map_translucency.vScaling,
                                                                        texture))
                                                                        
            if crn_mat.translucency[:] > (0.0, 0.0, 0.0) or (crn_mat.use_map_translucency and crn_mat.map_translucency.texture != ''):
                fw('#CORONA TranslucencyLevel %.2f\n' % crn_mat.translucency_level)

            if crn_mat.use_map_translucency_level and crn_mat.map_translucency_level.texture != '':
                if is_uv_img( crn_mat.map_translucency_level.texture):
                    texture = get_tex_path(crn_mat.map_translucency_level.texture)
                    fw('map_translucencyLevel ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_translucency_level.intensity, 
                                                                        crn_mat.map_translucency_level.uOffset,
                                                                        crn_mat.map_translucency_level.vOffset,
                                                                        crn_mat.map_translucency_level.uScaling,
                                                                        crn_mat.map_translucency_level.vScaling,
                                                                        texture))

            # Reflection
            fw('Ks %.2f %.2f %.2f\n' % ((crn_mat.ks * crn_mat.ns)[:])) 
            if crn_mat.use_map_ks and crn_mat.map_ks.texture != '':
                if is_uv_img( crn_mat.map_ks.texture):
                    texture = get_tex_path(crn_mat.map_ks.texture)
                    fw('map_Ks ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_ks.intensity, 
                                                                            crn_mat.map_ks.uOffset,
                                                                            crn_mat.map_ks.vOffset,
                                                                            crn_mat.map_ks.uScaling,
                                                                            crn_mat.map_ks.vScaling,
                                                                            texture))

            if crn_mat.use_map_ns and crn_mat.map_ns.texture != '':
                if is_uv_img( crn_mat.map_ns.texture):
                    texture = get_tex_path(crn_mat.map_ns.texture)
                    fw('map_Ns ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_ns.intensity, 
                                                                            crn_mat.map_ns.uOffset,
                                                                            crn_mat.map_ns.vOffset,
                                                                            crn_mat.map_ns.uScaling,
                                                                            crn_mat.map_ns.vScaling,
                                                                            texture))
                    
            fw( '#CORONA ReflectGlossiness %.2f\n' % crn_mat.reflect_glossiness)
            fw( '#CORONA ReflectFresnel %.2f\n' % crn_mat.reflect_fresnel)
            
            # Anisotropy
            if crn_mat.anisotropy > 0.0:
                fw( '#CORONA Anisotropy %.2f %.2f\n' % (crn_mat.anisotropy, crn_mat.aniso_rotation))

                if crn_mat.use_map_aniso and crn_mat.map_aniso.texture != '':
                    if is_uv_img( crn_mat.map_aniso.texture):
                        texture = get_tex_path(crn_mat.map_aniso.texture)
                        fw( 'map_aniso ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_aniso.intensity, 
                                                                            crn_mat.map_aniso.uOffset,
                                                                            crn_mat.map_aniso.vOffset,
                                                                            crn_mat.map_aniso.uScaling,
                                                                            crn_mat.map_aniso.vScaling,
                                                                            texture))
                if crn_mat.use_map_aniso_rot and crn_mat.map_aniso_rot.texture != '':
                    if is_uv_img( crn_mat.map_aniso_rot.texture):
                        texture = get_tex_path(crn_mat.map_aniso_rot.texture)
                        fw( 'map_anisorotation ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_aniso_rot.intensity, 
                                                                            crn_mat.map_aniso_rot.uOffset,
                                                                            crn_mat.map_aniso_rot.vOffset,
                                                                            crn_mat.map_aniso_rot.uScaling,
                                                                            crn_mat.map_aniso_rot.vScaling,
                                                                            texture))

            # Refraction
            # Write RefractMode even if refract level is 0, because it may be used by volumetrics, if enabled.
            if crn_mat.refract_level > 0.0 or crn_mat.scattering_albedo[:] > (0, 0, 0):
                if crn_mat.refract_caustics:
                    # If caustics is enabled
                    fw( '#CORONA RefractMode caustics\n')
                if crn_mat.refract_thin:
                    # If only thin is enabled
                    fw( '#CORONA RefractMode twosided\n')
                if not crn_mat.refract_thin and not crn_mat.refract_caustics:
                    # If neither are enabled
                    fw( '#CORONA RefractMode \n')
                    
            if crn_mat.refract_level > 0.0:
                refract_level = crn_mat.refract_level
                fw( '#CORONA Refract %.2f %.2f %.2f\n' % ( (crn_mat.refract * refract_level)[:]))
                if crn_mat.use_map_refract and crn_mat.map_refract.texture != '':
                    if is_uv_img( crn_mat.map_refract.texture):
                        texture = get_tex_path(crn_mat.map_refract.texture)
                        fw( 'map_refract ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_refract.intensity, 
                                                                            crn_mat.map_refract.uOffset,
                                                                            crn_mat.map_refract.vOffset,
                                                                            crn_mat.map_refract.uScaling,
                                                                            crn_mat.map_refract.vScaling,
                                                                            texture))
                            
                fw('Ni %.6f\n' % crn_mat.ni)  # Refraction index
                fw('#CORONA RefractGlossiness %.2f\n' % crn_mat.refract_glossiness)

            # Volumetrics
            if crn_mat.absorption_distance > 0.0:
                fw('#CORONA Attenuation %.4f %.4f %.4f ' % (crn_mat.absorption_color)[:])
                fw('%.8f \n' % crn_mat.absorption_distance)
                if crn_mat.use_map_absorption and crn_mat.map_absorption != '':
                    if is_uv_img( crn_mat.map_absorption.texture):
                        texture = get_tex_path(crn_mat.map_absorption.texture)
                        fw('map_attenuation ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_absorption.intensity, 
                                                                                crn_mat.map_absorption.uOffset,
                                                                                crn_mat.map_absorption.vOffset,
                                                                                crn_mat.map_absorption.uScaling,
                                                                                crn_mat.map_absorption.vScaling,
                                                                                texture))
                
            if crn_mat.scattering_albedo[:] > (0, 0, 0):
                fw( '#CORONA ScatteringAlbedo %.4f %.4f %.4f\n' % (crn_mat.scattering_albedo)[:])

            # Include scattering map even if scattering albedo is 0
            if crn_mat.use_map_scattering and crn_mat.map_scattering != '':
                if is_uv_img( crn_mat.map_scattering.texture):
                    texture = get_tex_path(crn_mat.map_scattering.texture)
                    fw('map_scatteringAlbedo ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_scattering.intensity, 
                                                                            crn_mat.map_scattering.uOffset,
                                                                            crn_mat.map_scattering.vOffset,
                                                                            crn_mat.map_scattering.uScaling,
                                                                            crn_mat.map_scattering.vScaling,
                                                                            texture))
                                                                            
            if crn_mat.scattering_albedo[:] > (0, 0, 0) or (crn_mat.use_map_scattering and crn_mat.map_scattering != ''):
                fw( '#CORONA MeanCosine %.4f\n' % crn_mat.mean_cosine)
                
            # Opacity
            fw('#CORONA Opacity %.2f %.2f %.2f\n' % (crn_mat.opacity)[:]) 
            if crn_mat.use_map_opacity and crn_mat.map_opacity.texture != '':
                if is_uv_img( crn_mat.map_opacity.texture):
                    texture = get_tex_path(crn_mat.map_opacity.texture)
                    fw('map_opacity ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_opacity.intensity, 
                                                                            crn_mat.map_opacity.uOffset,
                                                                            crn_mat.map_opacity.vOffset,
                                                                            crn_mat.map_opacity.uScaling,
                                                                            crn_mat.map_opacity.vScaling,
                                                                            texture))

            # Normal
            if crn_mat.use_map_normal and crn_mat.map_normal.texture != '':
                if is_uv_img( crn_mat.map_normal.texture):
                    texture = get_tex_path(crn_mat.map_normal.texture)
                    fw('#CORONA map_normal ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_normal.intensity, 
                                                                            crn_mat.map_normal.uOffset,
                                                                            crn_mat.map_normal.vOffset,
                                                                            crn_mat.map_normal.uScaling,
                                                                            crn_mat.map_normal.vScaling,
                                                                            texture))

            # Bump
            if crn_mat.use_map_bump and crn_mat.map_bump.texture != '':
                if is_uv_img( crn_mat.map_bump.texture):
                    texture = get_tex_path(crn_mat.map_bump.texture)
                    fw('#CORONA map_bump ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_bump.intensity, 
                                                                            crn_mat.map_bump.uOffset,
                                                                            crn_mat.map_bump.vOffset,
                                                                            crn_mat.map_bump.uScaling,
                                                                            crn_mat.map_bump.vScaling,
                                                                            texture))

            # Rounded corners
            if crn_mat.rounded_corners > 0.0:
                fw( '#CORONA RoundedCorners %.2f\n' % crn_mat.rounded_corners)

            # Ray invisibility
            inv_dict = { 'ray_gi_inv':'gi', 
                         'ray_direct_inv':'direct', 
                         'ray_reflect_inv':'reflect', 
                         'ray_refract_inv':'refract', 
                         'ray_shadows_inv':'shadows'}
            attr_list = [inv_dict[c] for c in inv_dict if getattr( crn_mat, c)]
            if len( attr_list) > 0:
                fw( 'Invisible ')
                for i in attr_list:
                    fw( '%s ' % i)
                fw( '\n')
            return

        elif crn_mat.mtl_type == 'coronalightmtl':
            emission = crn_mat.emission_mult
            fw( 'Kd 0 0 0\n')
            fw( 'Ke %.3f %.3f %.3f\n' % ( (crn_mat.ke * emission)[:]))
            fw( '#CORONA EmissionGlossiness %.3f\n' % crn_mat.emission_gloss)
            if crn_mat.use_map_ke and crn_mat.map_ke.texture != '':
                if is_uv_img( crn_mat.map_ke.texture):
                    texture = get_tex_path( crn_mat.map_ke.texture)
                    fw( 'map_Ke ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_ke.intensity, 
                                                                            crn_mat.map_ke.uOffset,
                                                                            crn_mat.map_ke.vOffset,
                                                                            crn_mat.map_ke.uScaling,
                                                                            crn_mat.map_ke.vScaling,
                                                                            texture))

            if crn_mat.use_ies and crn_mat.ies_profile != '':
                matrix = mathutils.Matrix.Identity(4)
                matrix = ' '.join([str(i) for m in matrix for i in m])
                fw( '#CORONA IesProfile %s %s %s\n' % ( realpath( crn_mat.ies_profile),
                                                      matrix, 
                                                      str(crn_mat.keep_sharp).lower()))
                
                 
            fw('#CORONA Opacity %.2f %.2f %.2f\n' % (crn_mat.opacity)[:]) 
            if crn_mat.use_map_opacity and crn_mat.map_opacity.texture != '':
                if is_uv_img( crn_mat.map_opacity.texture):
                    texture = get_tex_path(crn_mat.map_opacity.texture)
                    fw('map_opacity ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_opacity.intensity, 
                                                                            crn_mat.map_opacity.uOffset,
                                                                            crn_mat.map_opacity.vOffset,
                                                                            crn_mat.map_opacity.uScaling,
                                                                            crn_mat.map_opacity.vScaling,
                                                                            texture))
            # Ray invisibility
            inv_dict = { 'ray_gi_inv':'gi', 
                         'ray_direct_inv':'direct', 
                         'ray_reflect_inv':'reflect', 
                         'ray_refract_inv':'refract', 
                         'ray_shadows_inv':'shadows'}
            attr_list = [inv_dict[c] for c in inv_dict if getattr( crn_mat, c)]
            if len( attr_list) > 0:
                fw( 'Invisible ')
                for i in attr_list:
                    fw( '%s ' % i)
                fw( '\n')
            return

        elif crn_mat.mtl_type == 'coronavolumemtl':
            # Defaults
            fw( 'Kd 0 0 0\n')
            fw( '#CORONA Opacity 0 0 0\n')
            fw( '#CORONA RefractMode caustics\n')
            
            # Absorption
            fw('#CORONA Attenuation %.4f %.4f %.4f ' % (crn_mat.absorption_color)[:])
            fw('%.8f \n' % crn_mat.absorption_distance)
            if crn_mat.use_map_absorption and crn_mat.map_absorption != '':
                if is_uv_img( crn_mat.map_absorption.texture):
                    texture = get_tex_path(crn_mat.map_absorption.texture)
                    fw('map_attenuation ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_absorption.intensity, 
                                                                            crn_mat.map_absorption.uOffset,
                                                                            crn_mat.map_absorption.vOffset,
                                                                            crn_mat.map_absorption.uScaling,
                                                                            crn_mat.map_absorption.vScaling,
                                                                            texture))

            fw( '#CORONA ScatteringAlbedo %.4f %.4f %.4f\n' % (crn_mat.scattering_albedo)[:])
            fw( '#CORONA MeanCosine %.4f\n' % crn_mat.mean_cosine)

            # Include scattering map even if scattering albedo is 0
            if crn_mat.use_map_scattering and crn_mat.map_scattering != '':
                if is_uv_img( crn_mat.map_scattering.texture):
                    texture = get_tex_path(crn_mat.map_scattering.texture)
                    fw('map_scatteringAlbedo ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_scattering.intensity, 
                                                                            crn_mat.map_scattering.uOffset,
                                                                            crn_mat.map_scattering.vOffset,
                                                                            crn_mat.map_scattering.uScaling,
                                                                            crn_mat.map_scattering.vScaling,
                                                                            texture))
                                                                            
            # Emission
            emission = crn_mat.emission_mult
            fw( 'Ke %.3f %.3f %.3f\n' % ( (crn_mat.ke * emission)[:]))
            fw( '#CORONA EmissionGlossiness %.3f\n' % crn_mat.emission_gloss)
            if crn_mat.use_map_ke and crn_mat.map_ke.texture != '':
                if is_uv_img( crn_mat.map_ke.texture):
                    texture = get_tex_path( crn_mat.map_ke.texture)
                    fw( 'map_Ke ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_mat.map_ke.intensity, 
                                                                            crn_mat.map_ke.uOffset,
                                                                            crn_mat.map_ke.vOffset,
                                                                            crn_mat.map_ke.uScaling,
                                                                            crn_mat.map_ke.vScaling,
                                                                            texture))
            # Ray invisibility
            inv_dict = { 'ray_gi_inv':'gi', 
                         'ray_direct_inv':'direct', 
                         'ray_reflect_inv':'reflect', 
                         'ray_refract_inv':'refract', 
                         'ray_shadows_inv':'shadows'}
            attr_list = [inv_dict[c] for c in inv_dict if getattr( crn_mat, c)]
            if len( attr_list) > 0:
                fw( 'Invisible ')
                for i in attr_list:
                    fw( '%s ' % i)
                fw( '\n')
                
    else:
        params = node.get_node_params()
        fw( params)
        # Don't close the file - it will be closed by the calling function

#------------------------------------
# Function for writing global medium to mtl file
#------------------------------------
def write_global_medium( mat_file, scene):
    fw = mat_file.write
    crn_world = scene.world.corona

    if crn_world.absorption_distance > 0.0:
        fw('#CORONA Attenuation %.4f %.4f %.4f ' % (crn_world.absorption_color)[:])
        fw('%.8f \n' % crn_world.absorption_distance)
        if crn_world.use_map_absorption and crn_world.map_absorption != '':
            if is_uv_img( crn_world.map_absorption.texture):
                texture = get_tex_path(crn_world.map_absorption.texture)
                fw('map_attenuation ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_world.map_absorption.intensity, 
                                                                        crn_world.map_absorption.uOffset,
                                                                        crn_world.map_absorption.vOffset,
                                                                        crn_world.map_absorption.uScaling,
                                                                        crn_world.map_absorption.vScaling,
                                                                        texture))
                
    if crn_world.scattering_albedo[:] > (0, 0, 0):
        fw( '#CORONA ScatteringAlbedo %.4f %.4f %.4f\n' % (crn_world.scattering_albedo)[:])

    # Include scattering map even if scattering albedo is 0
    if crn_world.use_map_scattering and crn_world.map_scattering != '':
        if is_uv_img( crn_world.map_scattering.texture):
            texture = get_tex_path(crn_world.map_scattering.texture)
            fw('map_scatteringAlbedo ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_world.map_scattering.intensity, 
                                                                    crn_world.map_scattering.uOffset,
                                                                    crn_world.map_scattering.vOffset,
                                                                    crn_world.map_scattering.uScaling,
                                                                    crn_world.map_scattering.vScaling,
                                                                    texture))
                                                                    
    if crn_world.scattering_albedo[:] > (0, 0, 0) or (crn_world.use_map_scattering and crn_world.map_scattering != ''):
        fw( '#CORONA MeanCosine %.4f\n' % crn_world.mean_cosine)
    
#------------------------------------
# Material preview .stdin writing function.
#------------------------------------
def export_stdin( scene, 

                scene_file, 
                output_file, 
                prev_mat,
                width, 

                height, 
                bpy_materials, 
                bpy_textures):
    '''
    Write the .stdin file for preview rendering
    '''
    try:
        #mat_file = open(scene_file, 'w')
        with open(scene_file, 'w') as mat_file:
            fw = mat_file.write
            crn_mat = prev_mat.corona
            preview_quality = crn_mat.preview_quality
            fw("%.2f %d %d %s\n" % ( preview_quality, width, height, output_file))
            
            if crn_mat.node_tree == '' or crn_mat.node_output is None:
                # Write the properties from the material panel
                write_mtl( mat_file, False, prev_mat, None)
                return True
            else:
                # Write the Corona node tree
                node = bpy.data.node_groups[ crn_mat.node_tree].nodes[ crn_mat.node_output]
                write_mtl( mat_file, False, prev_mat, node)
                return True
        #mat_file.close()
    except:
        CrnError("Unable to write material preview. Check permissions on addons directories, and that texture formats are of supported type (PNG, EXR, BMP, JPG).")
        return False


#------------------------------------
# Configuration file writing function.
#------------------------------------
def write_conf(context, conf_file):
    '''
    Open and write the .conf file
    '''
    scene = context.scene
    crn_scn = scene.corona
    camera = scene.camera
    try:
        conf = open(conf_file, 'w')
        cw = conf.write
        CrnUpdate("Writing configuration file ", conf_file.split(os.path.sep)[-1])
        
        cw('  bool lowThreadPriority = %s\n'            % ('true' if crn_scn.low_threadpriority else 'false'))
        cw('  bool fb.showBucketOrder = %s\n'           % ('true' if crn_scn.vfb_show_bucket else 'false'))
        cw('  bool doShading = true\n')
        cw('  bool doAa = %s\n'                         % ('true' if crn_scn.do_aa else 'false'))
        cw('   int renderEngine = %s\n'                 % crn_scn.renderer_type)
        cw('   int accelerationStructure = 3\n')
        cw('   int gi.primarySolver = %s\n'             % crn_scn.gi_primarysolver)
        cw('   int gi.secondarySolver = %s\n'           % crn_scn.gi_secondarysolver)
        cw('   int fb.imageFilter = %s\n'               % crn_scn.image_filter)
        cw('   int lights.solver = 1\n')
        cw('   int lights.enviroSolver = 0\n')
        cw(' float lights.subdivEnviroThreshold = %.2f\n' % crn_scn.subdiv_env_threshold)
        cw(' float lights.texturedResolution = %.2f\n'  % crn_scn.lights_tex_res)
        cw('   int embree.triangles = 1\n')
        cw('   int random.sampler = 3\n')

        # Max passes
        max_passes = 0 if crn_scn.limit_prog_time else crn_scn.prog_max_passes
        cw('   int progressive.maxPasses = %d\n'        % max_passes)

        # Time limit
        hours = crn_scn.prog_timelimit_hour * 3600000 if crn_scn.limit_prog_time else 0
        mins = crn_scn.prog_timelimit_min * 60000 if crn_scn.limit_prog_time else 0
        secs = crn_scn.prog_timelimit_sec * 1000 if crn_scn.limit_prog_time else 0
        cw('   int progressive.timeLimit = %d\n'        % (hours + mins + secs))
        
        cw(' float lights.samplesMult = %.2f\n'         % crn_scn.arealight_samples)
        cw('   int lights.envResolution = %d\n'         % crn_scn.lights_env_res)
        cw('   int gi.giToAaRatio = %d\n'               % crn_scn.path_samples)
        cw('   int lights.areaMethod = %s\n'            % crn_scn.arealight_method)
        cw('   int maxRayDepth = %d\n'                  % crn_scn.max_depth)
        cw('   int fb.updateInterval = %d\n'            % crn_scn.vfb_update)

        # Check if we're using border render
        resX = scene.render.resolution_x * (scene.render.resolution_percentage / 100)
        resY = scene.render.resolution_y * (scene.render.resolution_percentage / 100)
        
        if not scene.render.use_border:
            # If not, write 0 to .conf file
            cw('   int image.region.startX = 0\n')
            cw('   int image.region.startY = 0\n')
            cw('   int image.region.endX = 0\n')
            cw('   int image.region.endY = 0\n')
            cw('   int image.width = %d\n'              % resX)
            cw('   int image.height = %d\n'             % resY)
        elif scene.render.use_border and not scene.camera.data.corona.use_region:
            cw('   int image.region.startX = %d\n'      % int(scene.render.border_min_x * resX))
            cw('   int image.region.startY = %d\n'      % int(scene.render.border_min_y * resY))
            cw('   int image.region.endX   = %d\n'      % int(scene.render.border_max_x * resX))
            cw('   int image.region.endY   = %d\n'      % int(scene.render.border_max_y * resY))
            cw('   int image.width = %d\n'              % int(resX))
            cw('   int image.height = %d\n'             % int(resY))
        
        cw('  Vec3 exitColor = %.2f %.2f %.2f\n'        % (crn_scn.ray_exit_color)[:])
        
        cw('   int fb.internalResolutionMult = %d\n'    % crn_scn.fb_res_mult)
        cw(' float fb.filterWidth = %.2f\n'             % crn_scn.filter_width)
        cw(' float fb.filterBlurring = %.2f\n'          % crn_scn.filter_blur)

        cw(' float maxNormalDiff = %.2f\n'              % crn_scn.max_normal_dev)
        cw('   int progressive.adaptivityInterval = %d\n' % crn_scn.prog_recalculate)
        cw(' float progressive.adaptivityAmount = %.2f\n' % crn_scn.prog_adaptivity)
        cw(' float maxSampleIntensity = %.2f\n'         % crn_scn.max_sample_intensity)
        cw('   int randomSeed = %d\n'                   % crn_scn.random_seed)
        
        threads = thread_count if crn_scn.auto_threads else crn_scn.num_threads 
        cw('   int numThreads = %d\n' % threads)
        
        cw(' float lightSolver.localFrac = 0.33\n')
        cw(' float lightSolver.globalFrac = 0.33\n')
        cw(' float lights.portalSampleFraction = %.2f\n' % crn_scn.portal_samples)
        cw(' float shadowBias = -6.07\n')
        cw('  bool resumeRendering = true\n')
        cw('   int minInstanceSaving     = 50000\n')
        
        cw('   int gi.hdCache.precalcMode = 0\n')
        cw(' float gi.hdCache.precompDensity = %.2f\n'  % crn_scn.hd_precomp_mult)
        cw(' float gi.hdCache.dirSensitivity = %.2f\n'  % crn_scn.hd_sens_direct)
        cw(' float gi.hdCache.posSensitivity = %.2f\n'  % crn_scn.hd_sens_position)
        cw('   int gi.hdCache.interpolationCount = %d\n'  % crn_scn.hd_interpolation_count)
        cw(' float gi.hdCache.normalSensitivity = %.2f\n' % crn_scn.hd_sens_normal)
        cw('   int gi.hdCache.recordQuality = %d\n'     % crn_scn.hd_pt_samples)
        cw(' float gi.hdCache.smoothing = %d\n'         % crn_scn.hd_smoothing)
        cw(' float gi.hdCache.glossyThreshold = %.2f\n' % crn_scn.hd_glossy_thresh)
        cw('   int gi.hdCache.maxRecords = %d\n'        % crn_scn.hd_max_records)
        cw('   int gi.hdCache.writablePasses = %d\n'    % crn_scn.hd_write_passes)
        cw('  bool gi.hdCache.save = %s\n'              % ('true' if crn_scn.save_secondary_gi and crn_scn.gi_secondarysolver == 3 else 'false'))
        if crn_scn.save_secondary_gi or crn_scn.load_secondary_gi:
            cw('string gi.hdCache.file = "%s"\n'            % realpath(crn_scn.gi_secondaryfile))
        cw('  bool gi.hdCache.doPreviz    = true\n')
        
        cw('   int gi.photons.emitted = %d\n'           % crn_scn.photons_emitted)
        cw('   int gi.photons.filter = %d\n'            % crn_scn.photons_filter)
        cw('  bool gi.photons.storeDirect = %s\n'       % ('true' if crn_scn.photons_store_direct else 'false'))
        cw('   int gi.photons.depth = %d\n'             % crn_scn.photons_depth)
        cw('   int gi.photons.lookupCount = %d\n'       % crn_scn.photons_lookup)
        
        cw('   int gi.vpl.emittedCount = %d\n'          % crn_scn.vpl_emitted_count)
        cw('   int gi.vpl.usedCount = %d\n'             % crn_scn.vpl_used_count)
        cw('   int gi.vpl.progressiveBatch = %d\n'      % crn_scn.vpl_progressive_batch)
        cw(' float gi.vpl.clamping = %.2f\n'            % crn_scn.vpl_clamping)
        
        cw('   int bucket.initialSamples = %d\n'        % crn_scn.buckets_samples)
        cw('   int bucket.passes     = %d\n'            % crn_scn.buckets_steps)
        cw(' float bucket.daptiveThreshold = %.2f\n'    % crn_scn.buckets_adaptive_threshold)
        cw('   int bucket.size = %d\n'                  % crn_scn.bucket_size)
        
        cw(' float bvh.costIteration = 1.0\n')
        cw(' float bvh.costTriangle = 1.0\n')
        cw('   int bvh.leafSizeMin = 2\n')
        cw('   int bvh.leafSizeMax = 6\n')
        
        cw(' float colorMapping.simpleExposure = %.2f\n'       % crn_scn.colmap_exposure)
        cw(' float colorMapping.gamma = %.2f\n'                % crn_scn.colmap_gamma)
        cw(' float colorMapping.highlightCompression = %.2f\n' % crn_scn.colmap_compression)
        cw('  bool colorMapping.usePhotographic = %s\n'        % ('true' if crn_scn.colmap_use_photographic else 'false'))
        cw(' float colorMapping.iso = %.3f\n'                    % crn_scn.colmap_iso)
        cw(' float colorMapping.dofFStop = %.2f\n'               % camera.data.corona.camera_dof)
        cw(' float colorMapping.shutterSpeed = %.3f\n'           % crn_scn.shutter_speed)
        cw(' float colorMapping.colorTemperature = %.2f\n'     % crn_scn.colmap_color_temp)
        cw(' float colorMapping.tint = %.2f %.2f %.2f\n'       % (crn_scn.colmap_tint[0],
                                                                  crn_scn.colmap_tint[1],
                                                                  crn_scn.colmap_tint[2]))
        cw(' float colorMapping.contrast = %.2f\n'             % crn_scn.colmap_contrast)
        
        cw('   int gi.ppm.samplesPerIter = %d\n'        % crn_scn.ppm_samples)
        cw('   int gi.ppm.photonsPerIter = %d\n'        % crn_scn.ppm_photons)
        cw(' float gi.ppm.alpha = %.2f\n'               % crn_scn.ppm_alpha)
        cw(' float gi.ppm.initialRadius = %.2f\n'       % crn_scn.ppm_initial_rad)
        cw('  bool gi.vcm.doMis = %s\n'                 % ("true" if crn_scn.bidir_mis else "false"))
        cw('   int gi.vcm.mode = %s\n'                  % crn_scn.vcm_mode)

#        Displacement isn't implemented yet.
#        cw('  bool displace.useProjectionSize = true\n')
#        cw(' float displace.maxProjectSize = 2\n')
#        cw(' float displace.maxWorldSize = 1\n')
#        cw('   int displace.maxSubdiv = 100\n')
        cw('  bool saveExr = %s\n'                      % ("true" if crn_scn.image_format == '.exr' else "false"))
        cw('  bool savePng = %s\n'                      % ("true" if crn_scn.image_format == '.png' else "false"))
        cw('  bool saveJpg = %s\n'                      % ("true" if crn_scn.image_format == '.jpg' else "false"))
        cw('  bool saveTiff     = %s\n'                 % ("true" if crn_scn.image_format == '.tiff' else "false"))
        cw('  bool saveTga = %s\n'                      % ("true" if crn_scn.image_format == '.tga' else "false"))
        cw('  bool saveAlpha    = %s\n'                 % str( crn_scn.save_alpha).lower())
        cw('  bool renderstamp.use = %s\n'              % ("true" if crn_scn.renderstamp_use else "false"))
        cw(r'string renderStamp = "Corona Renderer Alpha | %c | Time: %pt | Passes: %pp | Primitives: %si | Rays/s: %pr"')
        cw('\n   int vfb.type = %s\n'                     % crn_scn.vfb_type)

        #Close the file
        conf.close()
        return True
    except:
        return False


#------------------------------------
# Scene file writing function.
#------------------------------------
def write_scn( self, context, scn_file, conf_filename):
    '''
    Open and write the .scn file
    '''
    scene = context.scene
    crn_scn = scene.corona
    obj_ext = '.objb' if crn_scn.binary_obj else '.obj'
    camera = scene.camera
    width = scene.render.resolution_x
    height = scene.render.resolution_y
    SUCCESS = True
    FAILED = False
    try:
        CrnUpdate("Writing scene file ", scn_file.split(os.path.sep)[-1]) 
        scn = open(scn_file, 'w')
        scnw = scn.write
        
        scnw('# Automatic Corona scene export - main file\n')
        version = get_version_string()
        time_stamp = get_timestamp()
        scnw('# Generated by %s addon, %s, %s\n' % (script_name, version, time_stamp))
        scnw('conffile %s\n' % conf_filename)
        scnw('mtllib %s\n\n' % (scene.name.replace(' ', '_') + '.mtl'))

        self._no_export = {ob.corona.instance_mesh for ob in scene.objects if (ob.corona.is_proxy and not ob.corona.use_external)}    
        
        # Add all objects used as dupli_objects for particle systems
        self._no_export.update( {ob.name for ob in get_all_psysobs()})    
        
        for obj in scene.objects:
            self._psys_obs.clear()
            if do_export(obj, scene):
                if self._local_mode and in_local_layer(obj, self._local_layers):
                    debug( "Local mode", self._local_mode)
                    debug( obj.name)
                    write_transform( scn, scene, obj)
                    scnw('geometryGroup meshes\%s\n\n' % (obj.name.replace(' ', '_') + obj_ext))
                    continue
                    
                # Don't include dupli- child objects.
                elif obj.parent and obj.parent.dupli_type in {'VERTS', 'FACES', 'GROUP'}:
                    continue

                # Include the duplis themselves.
                elif obj.is_duplicator and obj.dupli_type in {'VERTS', 'FACES', 'GROUP'}:
                    self._dupli_obs.extend( get_instances(obj, scene))

                # Particle system emitter.
                elif is_psys_emitter( obj):
                    self._psys_obs = get_psys_instances(obj, scene)
                    debug( self._psys_obs)
                    #Include emitter geometry if set to render
                    if render_emitter( obj):
                        write_transform( scn, scene, obj)
                        scnw( 'geometryGroup meshes\%s\n\n' % (obj.name.replace(' ', '_') + obj_ext))

                # Proxy object.
                elif is_proxy(obj, scene):
                    if obj.corona.use_external:
                        self._ext_obs.append( [ realpath( obj.corona.external_mtllib),
                                        realpath( obj.corona.external_instance_mesh),
                                        obj])
                    else: 
                        self._dupli_obs.append([ scene.objects[obj.corona.instance_mesh], [obj.matrix_world]])

                elif not self._local_mode and obj.name not in self._no_export and not obj.corona.is_proxy:
                        write_transform( scn, scene, obj)
                        scnw('geometryGroup meshes\%s\n\n' % (obj.name.replace(' ', '_') + obj_ext))

            # Particle systems.
            if self._psys_obs and len( self._psys_obs) > 0:
                for ob in self._psys_obs:                
                    # each 'ob' is a particle, as dict key
                    # The value is a list, containing the dupli.object and another list of matrices
                    dupli = self._psys_obs[ob][0]       # The dupli.object
                    inst_mats = self._psys_obs[ob][1] # The list of matrices
                    
                    if is_proxy( dupli, scene) and dupli.corona.use_external:
                        self._ext_psys_obs.append( [ realpath( dupli.corona.external_mtllib),
                                        inst_mats,
                                        realpath( dupli.corona.external_instance_mesh)])
                    else:
                        if len( inst_mats) > 1:
                            scnw( 'transformA %d ' % crn_scn.ob_mblur_segments)
                        else:
                            scnw( 'transform ')
                        for mat in inst_mats:
                            # For each matrix in the list
                            for i in range( 0,3):
                                # For each vector in the matrix
                                scnw( '{0} {1} {2} {3} '.format(mat[i][0], 
                                                               mat[i][1], 
                                                               mat[i][2], 
                                                               mat[i][3]))
                        scnw('\n')
                        # Write the geometry name
                        scnw('geometryGroup meshes\%s\n\n' % (dupli.name.replace(' ', '_') + obj_ext))
                    
        # Duplis.
        if len( self._dupli_obs) > 0:
            for ob in self._dupli_obs:
                # [object, list of matrices]
                # Write the transform first
                dupli = ob[0]
                inst_mats = ob[1]
                if len( inst_mats) > 1:
                    scnw( 'transformA %d ' % crn_scn.ob_mblur_segments)
                else:
                    scnw( 'transform ')       
                for mat in inst_mats:
                    for i in range(0,3):
                        scnw( '{0} {1} {2} {3} '.format(mat[i][0], 
                                                       mat[i][1], 
                                                       mat[i][2], 
                                                       mat[i][3]))
                scnw('\n')
                # Write the geometry name
                scnw('geometryGroup meshes\%s\n\n' % (dupli.name.replace(' ', '_') + obj_ext))

        # External .objs.
        if len(self._ext_obs) > 0:
            for ob in self._ext_obs:
                mtllib = ob[0]
                inst_mesh = ob[1]
                dupli = ob[2]
                scnw( 'mtllib %s\n' % mtllib)
                write_transform( scn, scene, dupli)
                scnw( 'geometryGroup %s\n\n' % inst_mesh)

        # External psys .objs.
        if len( self._ext_psys_obs) > 0:
            for ob in self._ext_psys_obs:
                mtllib = ob[0]
                inst_mats = ob[1]
                inst_mesh = ob[2]

                scnw( 'mtllib %s\n' % mtllib)
                if len( inst_mats) > 1:
                    scnw( 'transformA %d ' % crn_scn.ob_mblur_segments)
                else:
                    scnw( 'transform ')
                for mat in inst_mats:
                    # For each matrix in the list
                    for i in range( 0,3):
                        # For each vector in the matrix
                        scnw( '{0} {1} {2} {3} '.format(mat[i][0], 
                                                       mat[i][1], 
                                                       mat[i][2], 
                                                       mat[i][3]))
                scnw('\n')
                # Write the geometry name
                scnw('geometryGroup %s\n\n' % inst_mesh)

                
        # Camera.
        crn_cam = camera.data.corona

        # Ortho camera.  
        if crn_cam.camera_type == 'ortho':
            if crn_scn.use_cam_mblur:
                scnw('camera ortho_atmw %d ' % (crn_scn.cam_mblur_segments + 1))
                write_transform( scn, scene, camera)
            else:
                scnw('camera ortho_tmw ')
                write_transform( scn, scene, camera)
            scnw(' %.2f \n' % (crn_cam.ortho_width * 4))

        # Perspective camera.
        else:
                if crn_scn.use_cam_mblur:
                    scnw('camera perspective_atmf %d ' % (crn_scn.cam_mblur_segments + 1))
                    write_transform( scn, scene, camera)
                else:
                    scnw( 'camera perspective_tmf ')
                    write_transform( scn, scene, camera)   

                # FOV.  
                scnw('%.2f ' % (calc_fov(camera, width, height)))

                if crn_cam.use_dof:
                    if camera.data.dof_object is not None:
                        focal_obj = bpy.data.objects[camera.data.dof_object.name]
                        focal_distance = (focal_obj.location - camera.location).magnitude
                    else:
                        focal_distance = camera.data.dof_distance 
                    scnw( 'focalDist %.3f ' % focal_distance)
                    scnw( 'fstop %.3f ' % crn_cam.camera_dof)
                    scnw( 'filmWidth %.3f ' % (camera.data.sensor_width / (1000 * scene.unit_settings.scale_length)))  # Convert to scene units

                    if crn_cam.bokeh_blades >= 3:
                        scnw( 'bokehPolygonal %d %.2f ' % ( crn_cam.bokeh_blades, math.degrees( crn_cam.bokeh_rotation)))
                    
                    if crn_cam.use_bokeh_img and crn_cam.bokeh_img != '':
                        scnw( 'bokehImg %s ' % realpath( crn_cam.bokeh_img))
                        
        if crn_cam.use_region and scene.render.use_border:
            resX = scene.render.resolution_x * (scene.render.resolution_percentage / 100)
            resY = scene.render.resolution_y * (scene.render.resolution_percentage / 100)
            scnw( 'region %f %f %f %f ' % ( scene.render.border_min_x * resX,
                                            scene.render.border_min_y * resY,
                                            scene.render.border_max_x * resX,
                                            scene.render.border_max_y * resY))
        if crn_cam.render_clipping:
            scnw( 'minT %.3f maxT %.3f ' % ( camera.data.clip_start, camera.data.clip_end))
            
        scnw( '\n\n')
            
        # Environment.
        if scene.world is not None:
            crn_world = scene.world.corona
            if crn_world.mode == 'color':        
                scnw( 'enviro color %.2f %.2f %.2f\n\n' % (scene.world.corona.enviro_color)[:])
            if crn_world.mode == 'latlong' and crn_world.enviro_tex != '':
                scnw( 'enviro texmap ":bitmap %.2f %.2f %.2f %.2f %.2f \\"%s\\""\n' % ( crn_world.latlong_intensity, crn_world.latlong_uOffset, crn_world.latlong_vOffset, crn_world.latlong_uScaling, crn_world.latlong_vScaling, realpath(crn_world.enviro_tex)))
            if crn_world.mode == 'sky':
                scnw( 'enviro texmap ":sky %.2f %.2f"\n' % ( crn_world.sky_intensity, crn_world.sky_turbidity))
            if crn_world.mode == 'rayswitch':
                gi_switch = '\\":solid %.3f %.3f %.3f\\"' % ( crn_world.gi_color[0], 
                                                              crn_world.gi_color[1],
                                                              crn_world.gi_color[2])
                                                              
                reflect_switch = '\\":solid %.3f %.3f %.3f\\"' % ( crn_world.reflect_color[0], 
                                                              crn_world.reflect_color[1],
                                                              crn_world.reflect_color[2])

                refract_switch = '\\":solid %.3f %.3f %.3f\\"' % ( crn_world.refract_color[0], 
                                                              crn_world.refract_color[1],
                                                              crn_world.refract_color[2])
                                                              
                direct_switch = '\\":solid %.3f %.3f %.3f\\"' % ( crn_world.direct_color[0], 
                                                              crn_world.direct_color[1],
                                                              crn_world.direct_color[2])

                if crn_world.gi_use_tex and crn_world.map_gi.texture != '':
                    map_gi = crn_world.map_gi
                    scnw( 'newmap GiSwitch ":bitmap %.3f %.3f %.3f %.3f %.3f \\"%s\\""\n' % (map_gi.intensity,
                                                                                      map_gi.uOffset,
                                                                                      map_gi.vOffset,
                                                                                      map_gi.uScaling,
                                                                                      map_gi.vScaling,
                                                                                      realpath( map_gi.texture)))
                    gi_switch = ':GiSwitch'
                if crn_world.reflect_use_tex and crn_world.map_reflect.texture != '':
                    map_reflect = crn_world.map_reflect
                    scnw( 'newmap ReflectSwitch ":bitmap %.3f %.3f %.3f %.3f %.3f \\"%s\\""\n' % (map_reflect.intensity,
                                                                                      map_reflect.uOffset,
                                                                                      map_reflect.vOffset,
                                                                                      map_reflect.uScaling,
                                                                                      map_reflect.vScaling,
                                                                                      realpath( map_reflect.texture)))
                    reflect_switch = ':ReflectSwitch'
                if crn_world.refract_use_tex and crn_world.map_refract.texture != '':
                    map_refract = crn_world.map_refract
                    scnw( 'newmap RefractSwitch ":bitmap %.3f %.3f %.3f %.3f %.3f \\"%s\\""\n' % (map_refract.intensity,
                                                                                      map_refract.uOffset,
                                                                                      map_refract.vOffset,
                                                                                      map_refract.uScaling,
                                                                                      map_refract.vScaling,
                                                                                      realpath( map_refract.texture)))
                    refract_switch = ':RefractSwitch'
                if crn_world.direct_use_tex and crn_world.map_direct.texture != '':
                    map_direct = crn_world.map_direct
                    scnw( 'newmap DirectSwitch ":bitmap %.3f %.3f %.3f %.3f %.3f \\"%s\\""\n' % (map_direct.intensity,
                                                                                      map_direct.uOffset,
                                                                                      map_direct.vOffset,
                                                                                      map_direct.uScaling,
                                                                                      map_direct.vScaling,
                                                                                      realpath( map_direct.texture)))
                    direct_switch = ':DirectSwitch'
                    
                scnw( 'enviro texmap ":rayswitch %s %s %s %s"\n' % ( gi_switch, reflect_switch, refract_switch, direct_switch))

            if crn_world.use_global_medium:
                # Use default name for global medium
                scnw( 'globalMedium _default_global_medium\n')
                
        else:
            CrnInfo( "No environment is specified! Add a new world in the World tab. Rendering with default environment.")
            scnw( 'enviro color 0.8 0.8 0.8\n\n')
        
        if scene.corona_sky.use_sun:
            if scene.corona_sky.sun_lamp == '':
                CrnInfo( "Sun lighting is enabled, but no sun lamp found in the scene!")
            else:
                sun = scene.objects[scene.corona_sky.sun_lamp]
                matrix = sun.matrix_world
                
                scnw('sun dirTo %s %s %s ' % (matrix[0][2], matrix[1][2], matrix[2][2]))
                
                sun_col = scene.corona_sky.sun_color

                energy = scene.corona_sky.sun_energy_mult * 1000

                scnw('color %.2f %.2f %.2f ' % ((sun_col * energy)[:]))
                scnw('sizeMult %.5f\n\n' % scene.corona_sky.sun_size_mult)
                
        
        # Render passes.
        attr_list = []
        components_list = ['diffuse', 
                            'reflect',

                            'refract',
                            'translucency',
                            'diffuse_indirect',
                            'reflect_indirect',
                            'refract_indirect',
                            'translucency_indirect',
                            'emission']
                            
        for item in scene.corona_passes.passes:
            if item:
                attr_list.clear()
                item_name = ('_').join(item.name.split())
                pass_type = item.render_pass
                if pass_type in {'ZDepth', 'Components', 'RawComponent', 'SourceColor', 'Id', 'Normals'}:
                    scnw( 'renderPass %s "%s" ' % (item.render_pass, item_name))
                    if pass_type == 'ZDepth':
                        attr_list.extend( [item.z_min, item.z_max])
                    if pass_type == 'Components':
                        attr_list = [c for c in components_list if getattr( item, c)]
                    if pass_type == 'RawComponent':
                        attr_list.append( item.components)
                    if pass_type == 'SourceColor':
                        attr_list.append( item.source_color)
                    if pass_type == 'Id':
                        attr_list.append( item.mask_id)
                    if pass_type == 'Normals':
                        attr_list.append( item.normals)
                    for attr in attr_list:
                        if isinstance( attr, str):
                            attr = attr.replace("_i", "I")
                        scnw('%s ' % attr)
                    scnw( '\n')
                else:
                    scnw('renderPass %s "%s"\n' % (item.render_pass, item_name))

        # Cleanup.
        scn.close()
        
        return SUCCESS
    except error as e:
        # This exception doesn't actually work, it just catches tracebacks
        return FAILED

    
