from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

modules = ['outputs', 'export']

for module in modules:
    ext_module = Extension(
        "%s" % module,
        ["%s.pyx" % module]
    )

    setup(
        name = 'Export module',
        cmdclass = {'build_ext': build_ext},
        ext_modules = [ext_module]
    )


