import bpy
import subprocess, os, time
import threading
from extensions_framework   import util as efutil
from .util                  import realpath, resolution, plugin_path, get_instance_materials
from .util                  import sep, CrnUpdate, CrnProgress, CrnError, CrnInfo, debug
from .outputs               import export_stdin

#------------------
# Generic Utilities
#------------------

def update_start( engine, data, scene):
    if engine.is_preview:
        update_preview( engine, data, scene)
    else:
        update_scene( engine, data, scene)

def render_start( engine, scene):
    if engine.is_preview:
        render_preview( engine, scene)
    else:
        if engine.animation:
            # Step through frames to render animation
            # animation can only be True if rendering with command line
            frame_start = scene.frame_start
            frame_current = frame_start
            scene.frame_set( frame_start)
            frame_end = scene.frame_end
            step = scene.frame_step
            while frame_current <= frame_end:
                render_scene( engine, scene)
                frame_current += frame_step
                scene.frame_set( frame_current)
        else:
            render_scene( engine, scene)

def render_init( engine):
    pass

def update_preview( engine, data, scene):
    pass

def update_scene( engine, data, scene):
    if os.path.isdir( realpath( scene.corona.export_path)):
        proj_name = None
    else:
        proj_name = ( scene.corona.export_path).split( sep)[-1]

#----------------------------------
# Render the material preview
#---------------------------------
def render_preview( engine, scene):
    if bpy.context.user_preferences.addons['render_corona'].preferences.corona_path == '':
        engine.report( {'INFO'}, 'Error: The binary path is unspecified! Check Corona addon user preferences.')
        return
    # Iterate through the preview scene, finding objects with materials attached
    objects_materials = {}
    (width, height) = resolution( scene)

    if (width, height) == (96, 96):
        return
    for object in [ob for ob in scene.objects if ob.is_visible( scene) and not ob.hide_render]:
        for mat in get_instance_materials( object):
	        if mat is not None:
		        if not object.name in objects_materials.keys(): objects_materials[object] = []
		        objects_materials[object].append( mat)

    # find objects that are likely to be the preview objects
    preview_objects = [o for o in objects_materials.keys() if o.name.startswith( 'preview')]
    if len( preview_objects) < 1:
        return

    # find the materials attached to the likely preview object
    likely_materials = objects_materials[preview_objects[0]]
    if len( likely_materials) < 1:
        return

    
    corona_path = realpath( bpy.context.user_preferences.addons['render_corona'].preferences.corona_path)
    corona_path = os.path.join( corona_path, "Corona_ReleaseLib.exe")
    
    tempdir = efutil.temp_directory()
    output_file = os.path.join( os.path.join( tempdir, "matpreview"), "matpreview.png")
    scene_file = os.path.join( os.path.join( tempdir,
        "matpreview"), "matpreview.stdin")
    output_path = os.path.join( tempdir, "matpreview")
    if not os.path.isdir( output_path):
        os.mkdir( output_path)
    pm = likely_materials[0]
    exporter = export_stdin( scene, scene_file, output_file, pm, width, height, bpy.data.materials, bpy.data.textures)
    if not exporter:
        CrnError( 'Error while exporting -- check the console for details.')
        return
    else:
        if not bpy.app.background:
            with open( scene_file,'r') as inf:
                cmd = ( corona_path, '-mtlPreview')
                corona_proc = subprocess.Popen( cmd,stdout=subprocess.PIPE,stdin=inf)
                returncode = corona_proc.communicate()
            if returncode[0] == b'.':
                if os.path.exists( output_file):
                    try:
                        result = engine.begin_result( 0, 0, width, height)
                        lay = result.layers[0]

                        lay.load_from_file( output_file)
                        engine.end_result( result)
                    except:
                        pass
                else:
                    err_msg = 'Error: Could not load render result from %s.' % engine.output_file
                    CrnError( err_msg)
                    
#----------------------------------
# Render and export the scene
#---------------------------------
def render_scene( engine, scene):	
    bpy.ops.corona.export_scene()
    bpy.ops.corona.export_mat()
    if scene.corona.obj_export_bool == True:
        bpy.ops.corona.export()
    
    DELAY = 0.1
    
    if scene.corona.export_path != '':
        render_dir = 'render' + sep if scene.corona.export_path[-1] == sep else sep + 'render' + sep
    else:
        CrnError( "Export path is not specified!  Set export path in Render tab, under Corona Render panel.")
        return
    render_output = os.path.join( realpath( scene.corona.export_path), render_dir)
    width = scene.render.resolution_x
    height = scene.render.resolution_y

    # Make the render directory, if it doesn't exist
    if not os.path.isdir( render_output):
        os.mkdir( render_output)

    try:
        for item in os.listdir( render_output):
            if item == scene.name.replace(' ', '_') + '_' + str( scene.frame_current) + scene.corona.image_format:
                os.remove( render_output + item)
    except:
        pass

    # Set filename to render
    filename = scene.name.replace(' ', '_') + ".scn"

    filename = os.path.join( realpath( scene.corona.export_path), filename)
    imagename = os.path.join( render_output, ( scene.name.replace(' ', '_') + str( scene.frame_current)))

    # Start the Corona executable
    # Get the absolute path to the executable dir
    corona_path = realpath( bpy.context.user_preferences.addons['render_corona'].preferences.corona_path)
    corona_path = os.path.join( corona_path, "Corona_ReleaseLib.exe")

    cmd = ( corona_path, filename, imagename)

    CrnUpdate( "Rendering scene file {0}...".format( cmd[1].split( '\\')[-1]))
    CrnUpdate( "Launching Corona Renderer...")
    process = subprocess.Popen( cmd, cwd = render_output, stdout=subprocess.PIPE)

    # The rendered image name and path
    render_image = imagename + scene.corona.image_format
    # Wait for the file to be created
    while not os.path.exists( render_image):
        if engine.test_break():
            try:
                process.kill()
            except:
                pass
            break

        if process.poll() != None:
            engine.update_stats( "", "Corona: Error")
            break

        time.sleep( DELAY)

    if os.path.exists( render_image):
        engine.update_stats( "", "Corona: Rendering")

        prev_size = -1

        def update_image():
            result = engine.begin_result( 0, 0, width, height)
            lay = result.layers[0]
            # possible the image wont load early on.
            try:
                lay.load_from_file( render_image)
            except:
                pass

            engine.end_result( result)

        # Update while rendering
        while True:
            if process.poll() != None:
                update_image()
                break

            #user exit
            if engine.test_break():
                try:
                    process.kill()
                except:
                    pass
                break

            # check if the file updated
            new_size = os.path.getsize( render_image)

            if new_size != prev_size:
                update_image()
                prev_size = new_size

            time.sleep( DELAY)

#----------------------------------------------
# Render engine/settings
#----------------------------------------------

class RenderCorona( bpy.types.RenderEngine):
    """Corona render engine class"""
    bl_idname = "CORONA"
    bl_label = "Corona"
    bl_use_preview = True

    render_lock = threading.Lock()

    animation = False
    
    def __init__( self):
        render_init( self)

    # final rendering
    def update( self, data, scene):
        update_start( self, data, scene)

    def render( self, scene):	   
        if self is None or scene is None:
	        CrnError( 'Scene is missing! Please select a scene to render')
	        return 
        if bpy.context.user_preferences.addons['render_corona'].preferences.corona_path == '':
	        CrnError( 'The binary path is unspecified! Check Corona addon user preferences.')
	        return
	
        with self.render_lock:	# just render one thing at a time
            render_start( self, scene)


    
