Update: render_corona is no longer in active development.  Please feel free to fork the repository and continue development if desired.

An export script from Blender to Corona Renderer Standalone - http://corona-renderer.com/

Installation:  
-Click the "Downloads" tab at the top of this page.  Download the .zip file of the latest version of the exporter and extract the contents.    
-Put the render_corona folder into your [blender install directory]bin/2.69/scripts/addons directory.  
-Enable the addon from the preferences panel, under the "Render" category  
-In the preferences panel, set the path to the Corona Standalone directory.  
-Select the renderer from the render engine dropdown.  
  
Please post issues/problems/feedback/requests in the "Issues" section of https://bitbucket.org/ohsnapitsjoel/render_corona  
  
Please consider donating to help further the development of the Blender to Corona exporter:   

  
[Donate via PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=W7BDBNM3PYJFY&lc=US&item_name=Blender%20to%20Corona%20development&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)


[On my blog](http://joelsartventures.blogspot.com/2014/02/blender-to-corona-exporter-development.html)
