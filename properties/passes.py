import bpy
from bpy.props import StringProperty, BoolProperty, FloatVectorProperty, PointerProperty
from bpy.props import FloatProperty, IntProperty, EnumProperty, CollectionProperty

#------------------------------------
# Render Pass properties
#------------------------------------
class CoronaRenderPasses( bpy.types.PropertyGroup):
    render_pass = EnumProperty( items = [
                                        ('Components', 'Components', ''),
                                        ('RawComponent', 'Raw Components', ''),
                                        ('SourceColor', 'Source Color', ''),
                                        ('Id', 'ID', ''),
                                        ('PrimitiveCoords', 'Primitive Coordinates', ''),
                                        ('MapCoords', 'Map Coordinates', ''),
                                        ('Normals', 'Normals', ''),
                                        ('NormalsDotProduct', 'Normals Dot Product', ''),
                                        ('Alpha', 'Alpha', ''),
                                        ('ZDepth', 'Z Depth', ''),
                                        ('NormalsDiscrepancy', 'Normals Discrepancy', ''),
                                        ('WorldPosition', 'World Position', ''),
                                        ('Shadows', 'Shadows', ''),
                                        ('Albedo', 'Albedo', '')],
                                        name = 'Render Pass', 
                                        description = "Enable render pass",
                                        default = 'Alpha')

    # Used only for ZDepth pass.
    z_min = FloatProperty( name = "Minimum", 
                                        description = "Z distance minimum",
                                        default = 0,
                                        min = 0.0,
                                        max = 1000)

    z_max = FloatProperty( name = "Maximum", 
                                        description = "Z distance maximum",
                                        default = 100,
                                        min = 0.0,
                                        max = 1000)

    # Options for Components passes.
    diffuse = BoolProperty( name = "Diffuse", 
                                        description = "Include direct diffuse material component",
                                        default = False)

    reflect = BoolProperty( name = "Reflect", 
                                        description = "Include direct reflection material component",
                                        default = False)

    refract = BoolProperty( name = "Refract", 
                                        description = "Include direct refraction material component",
                                        default = False)
                                        
    translucency = BoolProperty( name = "Translucency", 
                                        description = "Include direct translucency material component",
                                        default = False)

    diffuse_indirect = BoolProperty( name = "Diffuse", 
                                        description = "Include indirect diffuse material component",
                                        default = False)

    reflect_indirect = BoolProperty( name = "Reflect", 
                                        description = "Include indirect reflection material component",
                                        default = False)

    refract_indirect = refract = BoolProperty( name = "Refract", 
                                        description = "Include indirect refraction material component",
                                        default = False)
                                        
    translucency_indirect = BoolProperty( name = "Translucency", 
                                        description = "Include indirect translucency material component",
                                        default = False)

    emission = BoolProperty( name = "Emission", 
                                        description = "Include emission material component",
                                        default = False)

    # Options for RawComponents passes.
    components = EnumProperty( name = "Lighting Component",
                                        description = "Lighting component to include",
                                        items = [
                                        ('diffuse', 'Diffuse', ''),
                                        ('reflect', 'Reflect', ''),
                                        ('refract', 'Refract', ''),
                                        ('translucency', 'Translucency', '')],
                                        default = 'diffuse')

    normals = EnumProperty( name = "Normals Component",
                                        description = "Normals component to include",
                                        items = [
                                        ('shading', 'Shading', ''),
                                        ('geometry', 'Geometry', '')],
                                        default = 'shading')

    source_color = EnumProperty( name = "Source Color Component",
                                        description = "Source color component to include",
                                        items = [
                                        ('diffuse', 'Diffuse', ''),
                                        ('reflect', 'Reflect', ''),
                                        ('refract', 'Refract', ''),
                                        ('translucency', 'Translucency', ''),
                                        ('opacity', 'Opacity', '')],
                                        default = 'diffuse')

    mask_id = EnumProperty( name = "Mask ID",
                                        description = "Mask ID",
                                        items = [
                                        ('primitive', 'Primitive', ''),
                                        ('material', 'Material', ''),
                                        #('sourceNode', 'Source Node', ''),
                                        ('instance', 'Instance', '')],
                                        #('geometryGroup', 'Geometry Group', '')],
                                        default = 'primitive')
                                        

class CoronaRenderPassProps(bpy.types.PropertyGroup):
    passes = CollectionProperty( type = CoronaRenderPasses, 
                                        name = "Corona Render Passes", 
                                        description = "")
    pass_index = IntProperty( name = "Pass Index", 
                                        description = "", 
                                        default = 0, 
                                        min = 0, 
                                        max = 30)

def register():
    bpy.utils.register_class( CoronaRenderPasses)
    bpy.utils.register_class( CoronaRenderPassProps)
    bpy.types.Scene.corona_passes = PointerProperty( type = CoronaRenderPassProps)
def unregister():
    bpy.utils.unregister_class( CoronaRenderPassProps)
    bpy.utils.unregister_class( CoronaRenderPasses)
    
