import bpy
from bpy.props  import StringProperty, BoolProperty, FloatVectorProperty
from bpy.props  import FloatProperty, EnumProperty, IntProperty, PointerProperty
from ..util     import sun_enumerator

#------------------------------------
# World texture properties
#------------------------------------
class CoronaWorldTexProps( bpy.types.PropertyGroup):

    show_settings = BoolProperty( name = "Show Texture Settings",
                                description = "",
                                default = False)

    
    texture =  StringProperty( name = "Texture", 
                                description = "Texture to influence attribute - HDR format is not supported", 
                                default = "",
                                subtype = 'FILE_PATH')
                                
    intensity = FloatProperty( name = "Intensity", 
                                description = "Latitude-longitude map relative brightness",
                                default = 1.0,
                                min = 0.0,
                                soft_max = 5)
    
    uOffset = FloatProperty( name = "Offset U", 
                                description = "Map U offset",
                                default = 0,
                                min = 0.0,
                                max = 1)

    vOffset = FloatProperty( name = "Offset V", 
                                description = "Map V offset",
                                default = 0,
                                min = 0.0,
                                max = 1)

    uScaling = FloatProperty( name = "Scaling U", 
                                description = "Map U scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10)

    vScaling = FloatProperty( name = "Scaling V", 
                                description = "Map V scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10)
                                
#------------------------------------
# Environment properties
#------------------------------------
class CoronaWorldProps( bpy.types.PropertyGroup):
    mode = EnumProperty( items = [( 'color', 'Constant Color', 'Constant environment color'),
                                  ( 'latlong', 'Environment Texture', 'Latitude-longitude texture map. HDR format is not supported'),
                                  ( 'sky', 'Physical Sky', 'Hosek-Wilkie physical sky model'),
                                  ( 'rayswitch', 'Ray Switch', 'Switches between 4 maps based on the type of ray processed. Can be used to create materials that look differently when viewed directly from camera, in specular reflections, refractions, or in global illumination')],
                                name = "Environment",
                                description = "",
                                default = 'color')
                                
    enviro_color = FloatVectorProperty( name = "Environment Color", 
                                description = "Environment color", 
                                default = (0.65, 0.65, 0.8), 
                                min = 0.0, 
                                max = 1.0, 
                                subtype = 'COLOR')
    
    enviro_tex = StringProperty( name = "Latlong Map", 
                                description = "Texture to influence environment color. Only latitude-longitude environment maps may be used", 
                                default = "",
                                subtype = 'FILE_PATH')

    latlong_intensity = FloatProperty( name = "Intensity", 
                                description = "Latitude-longitude map relative brightness",
                                default = 1.0,
                                min = 0.0,
                                soft_max = 5)
    
    latlong_uOffset = FloatProperty( name = "Offset U", 
                                description = "Latitude-longitude map U offset",
                                default = 0,
                                min = 0.0,
                                max = 1)

    latlong_vOffset = FloatProperty( name = "Offset V", 
                                description = "Latitude-longitude map V offset",
                                default = 0,
                                min = 0.0,
                                max = 1)

    latlong_uScaling = FloatProperty( name = "Scaling U", 
                                description = "Latitude-longitude map U scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10)

    latlong_vScaling = FloatProperty( name = "Scaling V", 
                                description = "Latitude-longitude map V scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10)
    
    sky_intensity = FloatProperty( name = "Intensity", 
                                description = "Relative brightness of sky lighting",
                                default = 0.03,
                                min = 0,
                                soft_max = 1,
                                precision = 3)

    sky_turbidity = FloatProperty( name = "Turbidity",
                                description = "Amount of haze in the atmosphere",
                                default = 3,
                                min = 1.7, 
                                max = 10)

    # Volumetrics.
    use_global_medium = BoolProperty( name = "Global Volumetric Medium",
                                description = "Enable global volumetric medium",
                                default = False)

    absorption_distance = FloatProperty( name = "Distance", 
                                description = "Absorption distance of the volumetric medium (in scene units)", 
                                default = 0.001, 
                                min = 0.0, 
                                max = 1000, 
                                subtype = 'DISTANCE', 
                                unit = 'LENGTH', 
                                step = 4,
                                precision = 4)
    
    absorption_color = FloatVectorProperty( name = "Color", 
                                description = "Absorption color of the volumetric medium (the color a white ray will be after traveling the absorption distance into the medium)", 
                                default = (1.0, 1.0, 1.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)
                                
    scattering_albedo = FloatVectorProperty( name = "Scattering Color", 
                                description = "Sets the material volumetric scattering albedo. When set to non-zero value, volumetric rendering is enabled", 
                                default = (0.0, 0.0, 0.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)

    mean_cosine = FloatProperty( name = "Directionality", 
                                description = "Sets the volumetric scattering anisotropy. Value 0 means isotropic (=diffuse) scattering, value 0.999 means almost perfect forward scattering, value -0.999 means almost perfect backward scattering",
                                default = 0,
                                min = -0.999,
                                max = 0.999)

    use_map_scattering = BoolProperty( name = "Use Scattering Albedo Texture",
                                description = "Use a texture to influence scattering albedo", 
                                default = False)

    use_map_absorption = BoolProperty( name = "Use Absorption Texture",
                                description = "Use a texture to influence absorption", 
                                default = False)

    map_scattering = PointerProperty( type = CoronaWorldTexProps)

    map_absorption = PointerProperty( type = CoronaWorldTexProps)
                                
    # Ray Switch overrides.                  
    gi_color = FloatVectorProperty( name = "GI Color", 
                                description = "GI color", 
                                default = (0.8, 0.8, 0.8), 
                                min = 0.0, 
                                max = 1.0, 
                                subtype = 'COLOR')

    reflect_color = FloatVectorProperty( name = "Reflect Color", 
                                description = "Reflect color", 
                                default = (0.8, 0.8, 0.8), 
                                min = 0.0, 
                                max = 1.0, 
                                subtype = 'COLOR')

    refract_color = FloatVectorProperty( name = "Refract Color", 
                                description = "Refract color", 
                                default = (0.8, 0.8, 0.8), 
                                min = 0.0, 
                                max = 1.0, 
                                subtype = 'COLOR')

    direct_color = FloatVectorProperty( name = "Direct Color", 
                                description = "Direct color", 
                                default = (0.8, 0.8, 0.8), 
                                min = 0.0, 
                                max = 1.0, 
                                subtype = 'COLOR')

    gi_use_tex = BoolProperty( name = "Use Texture",
                                description = "Use a texture map to influence color of environment in global illumination",
                                default = False)

    reflect_use_tex = BoolProperty( name = "Use Texture",
                                description = "Use a texture map to influence color of environment in reflections",
                                default = False)

    refract_use_tex = BoolProperty( name = "Use Texture",
                                description = "Use a texture map to influence color of environment in refractions",
                                default = False)
                                
    direct_use_tex = BoolProperty( name = "Use Texture",
                                description = "Use a texture map to influence color of environment when viewed from camera",
                                default = False)

    map_gi = PointerProperty( type = CoronaWorldTexProps)

    map_reflect = PointerProperty( type = CoronaWorldTexProps)

    map_refract = PointerProperty( type = CoronaWorldTexProps)

    map_direct = PointerProperty( type = CoronaWorldTexProps)

                                
#------------------------------------
# Sun/sky properties
#------------------------------------
class CoronaSunSettings( bpy.types.PropertyGroup):

    use_sun = BoolProperty( name = "", 
                                        description = 'Use Corona Sun', 
                                        default = False)
    
    sun_lamp = EnumProperty( items = sun_enumerator, 
                                        name = "Sun Lamp", 
                                        description = "Sun lamp to export")
    
    sun_size_mult = FloatProperty( name = "Size Multiplier", 
                                        description = "Sun size multiplier", 
                                        default = 1.0, 
                                        min = 0.001, 
                                        max = 20.0, 
                                        precision = 5)
    
    sun_color = FloatVectorProperty( name = "Sun Color", 
                                        description = "Sun light color", 
                                        default = (0.8, 0.8, 0.8), 
                                        min = 0.0, 
                                        max = 1.0, 
                                        subtype = "COLOR")
    
    sun_energy_mult = IntProperty( name = "Energy Multiplier", 
                                        description = "Sun energy multiplier -- energy compensation for low sun sizes", 
                                        default = 1, 
                                        min = 1, 
                                        max = 1000)

    
    
def register():
    bpy.utils.register_class( CoronaSunSettings)
    bpy.utils.register_class( CoronaWorldTexProps)
    bpy.utils.register_class( CoronaWorldProps)
    bpy.types.Scene.corona_sky = PointerProperty( type  =  CoronaSunSettings)
    bpy.types.World.corona = PointerProperty( type = CoronaWorldProps)
def unregister():
    bpy.utils.unregister_class( CoronaSunSettings)
    bpy.utils.unregister_class( CoronaWorldProps)
    bpy.utils.unregister_class( CoronaWorldTexProps)
