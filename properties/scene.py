import bpy
from bpy.props  import StringProperty, BoolProperty, FloatVectorProperty
from bpy.props  import IntProperty, FloatProperty, EnumProperty, PointerProperty
from ..util     import mat_enumerator, random_seed, thread_count

def update_display_mode( self, context):
    scene = context.scene
    if scene.corona.local_view_only:
        # Save current display mode
        scene.corona.display_mode_temp = scene.render.display_mode
        scene.render.display_mode = 'NONE'
    else:
        scene.render.display_mode = scene.corona.display_mode_temp

#------------------------------------
# Render properties
#------------------------------------
class CoronaRenderSettings( bpy.types.PropertyGroup):

    save_alpha = BoolProperty( name = "Transparent Background",
                                        description = "Render background as transparent",
                                        default = True)
    # Motion blur.
    use_ob_mblur = BoolProperty( name = "Enable Transformation",
                                        description = "Enable rendering of rigid (transformation) motion blur",
                                        default = False)
                                        
    use_def_mblur = BoolProperty( name = "Enable Geometry",
                                        description = "Enable rendering of non-rigid (deformation) motion blur",
                                        default = False)

    use_cam_mblur = BoolProperty( name = "Enable Camera",
                                        description = "Enable rendering of camera motion blur",
                                        default = False)
        
    ob_mblur_segments = IntProperty( name = "Transform Segments",
                                        description = "Quality of non-linear rigid (transformation) motion blur",
                                        default = 4, 
                                        min = 1,
                                        max = 16)
                                        
    def_mblur_segments = IntProperty( name = "Geometry Segments",
                                        description = "Quality of non-linear non-rigid (deformation) motion blur. Increasing this value increases quality at the expense of some render speed and memory usage",
                                        default = 1,
                                        min = 1,
                                        max = 8)

    cam_mblur_segments = IntProperty( name = "Camera Segments",
                                        description = "Quality of non-linear rigid (transformation) motion blur",
                                        default = 4, 
                                        min = 1,
                                        max = 16)
    
    shutter_speed = FloatProperty( name = "Shutter Speed 1/",
                                        description = "Camera shutter speed in 1/x seconds. For photographic results, use standard values such as 1/30, 1/60, etc.",
                                        default = 50,
                                        min = 0.001,
                                        max = 999999)

    frame_offset = FloatProperty( name = "Frame Offset", 
                                        description = "Exposure start time offset.  A value of 1 starts exposure at the current frame",
                                        default = 0,
                                        min = -1, 
                                        max = 1)
                                        
    limit_prog_time = BoolProperty( name = "Use Progressive Time Limit",
                                        description = "Set a time limit on progressive rendering, rather than using a maximum passes limit",
                                        default = False)
    image_format = EnumProperty( name = "File Format",
                                        description = "File format to save the rendered images as",
                                        items = [
                                        ('.exr', 'EXR', 'Output images in EXR format'),
                                        ('.png', 'PNG', 'Output images in PNG format'),
                                        ('.jpg', 'JPEG', 'Output images in JPEG format'),
                                        ('.tiff', 'TIFF', 'Output images in TIFF format'),
                                        ('.tga', 'Targa', 'Output images in Targa format')],
                                        default = '.png')
                                        
    export_hair = BoolProperty( name = "Export Hair", 
                                        description = "Export hair particle systems as renderable geometry", 
                                        default = False)

    material_override = BoolProperty( name = "Material Override", 
                                        description = "Override all materials with selected shader", 
                                        default = False)
    
    clay_render = BoolProperty( name = "Clay Render", 
                                        description = "Render all materials as default diagnostic shader", 
                                        default = False)

    override_material = EnumProperty( name = "Material", 
                                        description = "Use this material as override shader", 
                                        items = mat_enumerator)
    
    binary_obj = BoolProperty( name = "Binary OBJ", 
                                        description = "Export objects as binary '.objb' format", 
                                        default = False)
    
    obj_export_mode = EnumProperty( name = "Mode", 
                                        description = "Geometry export mode", 
                                        items = [
                                        ('ALL', 'All', 'Export all renderable objects, overwriting files if any exist.'),
                                        ('PARTIAL', 'Partial', 'Export only renderable objects that have not been previously written to disk. Existing files will not be overwritten.'),
                                        ('SELECTED', 'Only Selected', 'Export only the selected objects, overwriting files if any exist.')],
                                        default = 'ALL')
    
    obj_export_bool = BoolProperty( name = "Overwrite Geometry", 
                                        description = "Export geometry when rendering. Disable if no changes have been made to geometry and objects are not animated", 
                                        default = True)

    local_view_only = BoolProperty( name = "Render Local Mode",
                                        description = "Render only objects in local view if using local view mode. Display mode will be set to 'Keep UI'.  There must be a 3D View window open to render the local view",
                                        default = False,
                                        update = update_display_mode)

    display_mode_temp = StringProperty()
                                        
    save_secondary_gi = BoolProperty( name= "Save Secondary GI", 
                                        description = "Save secondary GI", 
                                        default = False)
    
    load_secondary_gi = BoolProperty( name = "Load Secondary GI", 
                                        description = "Load secondary GI", default = False)
    
    gi_primaryfile = StringProperty( name = "Primary GI File", 
                                        description = "Location of primary GI file", default = "C:/primaryGI.dat", subtype = "FILE_PATH")

    gi_secondaryfile = StringProperty( name = "Secondary GI File", 
                                        description = "Location of secondary GI file", default = "C:/aa.dat", subtype = "FILE_PATH")
    
    low_threadpriority = BoolProperty( name = "Low Thread Priority", 
                                        description = "Low thread priority", default = True)

    vfb_show_bucket = BoolProperty( name = "Show Bucket Order", 
                                        description = "Show bucket order in virtual frame buffer", default = False)

    do_shading = BoolProperty( name = "Use Shading", 
                                        description = "Use material shading settings - otherwise, render without shading (for fast visualization)", default = True)

    do_aa = BoolProperty( name = "Use Anti-aliasing", 
                                        description = "Use anti-aliasing", default = True)

    renderer_type = EnumProperty( items = [
                                        ('0', 'Bucket', 'Bucket renderer'),
                                        ('2', 'Progressive', 'Progressive renderer'),
                                        ('3', 'Bidir - VCM', 'Bidirectional - Vertex Connection Merging'),
                                        ('4', 'Simple PT', 'Simple path-tracing - no next-event estimation'),
                                        ('5', 'VPL', 'Virtual point lights'),
                                        ('6', 'PPM', 'Progressive photon mapping')],
                                        name = "Renderer",
                                        description = "Renderer type",
                                        default = '2')
    
    #Leave Irradiance caching out right now - no parameters to write
    gi_primarysolver = EnumProperty( items = [
                                        ('0', 'None', 'No global illumination'),
                                        ('1', 'Path Tracing', 'Brute force per-pixel global illumination')],
                                        name = "GI Primary Solver",
                                        description = "Primary global illumination solver",
                                        default = '1')

    gi_secondarysolver = EnumProperty( items = [
                                        ('1', 'Path Tracing', 'Brute force per-pixel global illumination'),
                                        ('3', 'HD Cache', 'Radiance cache')],
                                        name = "GI Secondary Solver",
                                        description = "Secondary global illumination solver",
                                        default = '3')

    image_filter = EnumProperty( items = [
                                        ('0', 'None', 'No image filtering'),
                                        ('1', 'Box', 'Box filtering'),
                                        ('2', 'Gaussian', 'Gaussian filtering'),
                                        ('3', 'Tent', 'Tent filtering')],
                                        name = "Image Filter",
                                        description = "Image filtering to reduce noise",
                                        default = '3')

    filter_width = FloatProperty( name = "Filter Width", 
                                        description = "Image filter width (in pixels)", default = 1.5, min = 0.0, max = 3.0)
    filter_blur = FloatProperty( name = "Filter Blurring", 
                                        description = "Image filter blurring", default = 0.5, min = 0.0, max = 1.0)

    embree_tris = EnumProperty( items = [
                                        ('0', 'Embree Fast', 'Embree Fast'),
                                        ('1', 'Embree AVX', 'Embree AVX')],
                                        name = "Embree Triangles",
                                        description = "Embree triangles",
                                        default = '0')

    prog_max_passes = IntProperty( name = "Max Passes", 
                                        description = "Progressive rendering maximum passes", 
                                        default = 0, 
                                        min = 0, 
                                        max = 9999)

    prog_timelimit_hour = IntProperty( name = "h", 
                                        description = "Progressive rendering time limit per frame (in hours). Default of 0 in all fields disables time limit.", 
                                        default = 0, 
                                        min = 0 )
    
    prog_timelimit_min = IntProperty( name = "m", 
                                        description = "Progressive rendering time limit per frame (in minutes). Default of 0 in all fields disables time limit.", 
                                        default = 0, 
                                        min = 0 )
    
    prog_timelimit_sec = IntProperty( name = "s", 
                                        description = "Progressive rendering time limit per frame (in seconds). Default of 0 in all fields disables time limit.", 
                                        default = 0, 
                                        min = 0 )

    arealight_samples = FloatProperty( name = "Area Light Sample Multiplier", 
                                        description = "Mulitplies number of samples devoted to sampling area lights relative to other sampling", 
                                        default =2.0, 
                                        min = 0.1, 
                                        max = 100)

    arealight_method = EnumProperty( items = [
                                        ('0', 'Area Light Simple', ''),
                                        ('1', 'Area Light Re-project', '')],
                                        name = "Area Light Method",
                                        description = "",
                                        default = '1')

    path_samples = IntProperty( name = "Path Tracing Samples", 
                                        description = "Number of rays traced per sample", 
                                        default = 16, 
                                        min = 1, 
                                        max = 1024)

    max_depth = IntProperty( name = "Max Ray Depth", 
                                        description = "Maximum ray depth", 
                                        default = 25, 
                                        min = 0, 
                                        max = 100)

    min_depth = IntProperty( name = "Min Ray Depth", 
                                        description = "Minimum ray depth", 
                                        default = 0,
                                        min = 0, 
                                        max = 100)

    vfb_update = IntProperty( name = "Frame Buffer Update Interval", 
                                        description = "Virtual frame buffer update interval in miliseconds", 
                                        default = 1000, 
                                        min = 0, 
                                        max = 3600000)

    bucket_size = IntProperty( name = "Bucket Size", 
                                        description = "Bucket render bucket size", 
                                        default = 32, 
                                        min = 1, 
                                        max = 1024)

    ray_exit_color = FloatVectorProperty( name = "Ray Exit Color", 
                                        description = "Color a ray returns after reaching maximum trace depth", 
                                        default = (0.0, 0.0, 0.0), 
                                        subtype = "COLOR")

    fb_res_mult = IntProperty( name = "Internal Resolution Multiplier", 
                                        description = "Multiplier of interally computed output resolution", 
                                        default = 2, 
                                        min = 1, 
                                        max = 4)

    max_normal_dev = FloatProperty( name = "Max Normal Difference", 
                                        description = "Maximum acceptable difference between geometric and shading normal", 
                                        default = 0.55, 
                                        min = 0.0, 
                                        max = 2.0)
    
    prog_recalculate = IntProperty( name = "Adaptivity Interval", 
                                        description = "Recalculate every nth pass", 
                                        default = 0, 
                                        min = 0, 
                                        max = 999)

    prog_adaptivity = FloatProperty( name = "Adaptivity Amount", 
                                        description = "Progressive adaptivity", 
                                        default = 0.0,
                                        min = 0.0, 
                                        max = 99)

    max_sample_intensity = FloatProperty( name = "Max Sample Intensity", 
                                        description = "Reduce fireflies at the cost of accuracy -- lower values = larger bias. A setting of 0 disables this feature = unbiased path tracing", 
                                        default = 20.0, 
                                        min = 0.0, 
                                        max = 99999)

    subdiv_env_threshold = FloatProperty( name= "Subdiv Threshold", 
                                        description = "Maximum difference of color on textured lights that can be merged with likely colors in order to optimize segmentation of environment lighting - DO NOT TOUCH unless you know what you are doing.", 
                                        default = 0.005, 
                                        min = 0.0, 
                                        max = 1.0)

    lights_tex_res = FloatProperty( name = "Texture Resolution", 
                                        description = "Resolution of illumination generated by textured lights", 
                                        default = 0.3, 
                                        min = 0.0001, 
                                        max = 99)
    
    lights_env_res = IntProperty( name = "Environment Resolution", 
                                        description = "Environment Resolution", 
                                        default = 512, 
                                        min = 0, 
                                        max = 100000)

    enviro_solver = EnumProperty( items = [
                                        ('0', 'Fast', ''),
                                        ('1', 'Fast Compensate', '')],
                                        name = "Environment Solver",
                                        description = "",
                                        default = '0')

    vfb_aa = IntProperty( name = "Frame Buffer AA", 
                                        description = "Virtual framebuffer display antialiasing when displaying image at scale < 1:1", 
                                        default = 0, 
                                        min = 0)

    random_seed = IntProperty( name = "Random Seed", 
                                        description = "Random seed", 
                                        default = random_seed(), 
                                        min = 1, 
                                        max = 2000)

    num_threads = IntProperty( name = "Render Threads", 
                                        description = "Number of threads to use for rendering", 
                                        default = 8, 
                                        min = 1, 
                                        max = thread_count)

    auto_threads = BoolProperty( name = "Auto", 
                                        description = "Automatic CPU thread detection", 
                                        default = True)

    
    solid_alpha = BoolProperty( name = "Material Solid Alpha", 
                                        description = "All materials have solid alpha, regardless of opacity settings", 
                                        default = False)
    
    portal_samples = FloatProperty( name = "Portal Samples Fraction", 
                                        description = "", 
                                        default = 0.75, 
                                        min = 0.001, 
                                        max = 1.0, 
                                        precision = 3)

    # HD Cache parameters
    hd_precomp_mult = FloatProperty( name = "Precomputation Density", 
                                        description = "", 
                                        default = 1.00, 
                                        min = 0.01, 
                                        max = 10 )

    hd_interpolation_count = IntProperty( name = "Interpolation Count", 
                                        description = "Maximum number of samples used for interpolation of HD Cache", 
                                        default = 3, 
                                        min = 1, 
                                        max = 512)

    hd_sens_direct = IntProperty( name = "Direct", 
                                        description = "Directional sensitivity of HD cache samples interpolation", 
                                        default = 2, 
                                        min = 1, 
                                        max = 20)

    hd_sens_position = IntProperty( name = "Position", 
                                        description = "Positional sensitivy of HD cache samples interpolation", 
                                        default = 20, 
                                        min = 1, 
                                        max = 20)

    hd_sens_normal = IntProperty( name = "Normal", 
                                        description = "Normal sensitivity of HD cache samples interpolation", 
                                        default = 3, 
                                        min = 1, 
                                        max = 20)

    hd_pt_samples = IntProperty( name = "Record Quality", 
                                        description = "Quality of HD cache interpolation - higher values eliminate artifacts and animation flickering", 
                                        default = 256, 
                                        min = 1, 
                                        max = 2048)

    hd_smoothing = IntProperty( name = "Smoothing", 
                                        description = "HD cache samples interpolation smoothing", 
                                        default = 2, 
                                        min = 1, 
                                        max = 10)

    hd_glossy_thresh = FloatProperty( name = "Glossy Threshold", 
                                        description = "Glossiness threshold to decide between ray tracing glossy reflections and using HD cache to interpolate them", 
                                        default = 0.9, 
                                        min = 0.1, 
                                        max = 1.0)

    hd_max_records = IntProperty( name = "Max Records", 
                                        description = "Maximum count of HD cache records in thousands. Higher values can add slightly to GI computation time but limited values cause missing records to get replaced by path tracing = longer convergence time", 
                                        default = 100000, 
                                        min = 1, 
                                        max = 100000)

    hd_write_passes = IntProperty( name = "Writeable for Passes", 
                                        description = "Number of passes for which HD cache will be used to write new samples. 0 = precomputation phase only, 1 = precomputation and first pass, etc.", 
                                        default = 0)

    # Photon mapping parameters
    photons_emitted = IntProperty( name = "Emitted Photons",
                                        description = "Total number of photons emitted into the scene", 
                                        default = 100000, 
                                        min = 50)

    photons_store_direct = BoolProperty( name = "Store Direct Lighting", 
                                        description = "Enable storage of direct lighting within photon map", 
                                        default = True)

    photons_depth = IntProperty( name = "Depth", 
                                        description = "Maximum photon depth", 
                                        default = 1, 
                                        min = 1, 
                                        max = 256)

    photons_lookup = IntProperty( name = "Lookup Count", 
                                        description = "How many photons will be interpolated when final information from photon map is extracted", 
                                        default = 50, 
                                        min = 1, 
                                        max = 512)
    
    # Not exposed to the UI yet
    photons_filter = IntProperty( name = "Filter", 
                                        description = "", 
                                        default = 1, 
                                        min = 0, 
                                        max = 4)

    # What do these VPL values mean?
    vpl_emitted_count = IntProperty( name = "Emitted Count", 
                                        description = "", 
                                        default = 100000, 
                                        min = 50)

    vpl_used_count = IntProperty( name = "Used Count", 
                                        description = "", 
                                        default = 50, 
                                        min = 1)

    vpl_progressive_batch = IntProperty( name = "Progressive Batch", 
                                        description = "", 
                                        default = 1, 
                                        min = 1, 
                                        max = 9999)

    vpl_clamping = FloatProperty( name = "Clamping", 
                                        description = "Clamping of white pixel values", 
                                        default = 0.0, 
                                        min = 0.0, 
                                        max = 1.0)

    # Adaptive AA parameters
    buckets_samples = IntProperty( name = "Initial Samples", 
                                        description = "Number of antialising subdivisions when using bucket rendering", 
                                        default = 1, 
                                        min = 1, 
                                        max = 999)

    buckets_steps = IntProperty( name = "Passes", 
                                        description = "Additional passes to improve antialising", 
                                        default = 2, 
                                        min = 1, 
                                        max = 10)

    buckets_adaptive_threshold = FloatProperty( name = "Threshold", 
                                        description = "Error threshold under which antialising will not refine further", 
                                        default = 0.03, 
                                        min = 0.001, 
                                        max = 1.0)
    
    colmap_compression = FloatProperty( name = "Highlight Compression", 
                                        description = "Controls compression of highlights", 
                                        default = 1.0, 
                                        min = 0.01, 
                                        max = 999.0)

    colmap_exposure = FloatProperty( name = "Exposure", 
                                        description = "Controls brightness of the image - increasing = brighter image, decreasing = darker image", 
                                        default = 0.0, 
                                        min = -20.0, 
                                        max = 20.0)

    colmap_iso = FloatProperty( name = "ISO",
                                        description = "Film speed",
                                        default = 100,
                                        min = 0.001,
                                        max = 999999)

    colmap_gamma = FloatProperty( name = "Gamma", 
                                        description = "Gamma correction of output image", 
                                        default = 2.2, 
                                        min = 0.1, 
                                        max = 10.0)

    colmap_color_temp = FloatProperty( name = "White Balance [K]", 
                                        description = "Controls white balance balance of the image output (values in Kelvins) - values toward the left are warmer, values toward right are cooler", 
                                        default = 6500.0, 
                                        min = 1800.0, 
                                        max = 99999.0)

    colmap_tint = FloatVectorProperty( name = "Color Tint", 
                                        description = "RGB color balance of output image", 
                                        default = (1.0, 1.0, 1.0), 
                                        subtype = "COLOR", 
                                        min = 0.0, 
                                        max = 1)

    colmap_contrast = FloatProperty( name = "Contrast", 
                                        description = "Image contrast", 
                                        default = 1.0, 
                                        min = 1.0, 
                                        max = 99)

    colmap_use_photographic = BoolProperty( name = "Use Photographic Exposure",
                                        description = "When enabled, photographic exposure parameters in camera settings will be used to control exposure.  When disabled, exposure value below will be used to control exposure",
                                        default = False)
                                        
    # PPM
    ppm_samples = IntProperty( name = "Samples Per Iteration", 
                                        description = "Number of samples per pass", 
                                        default = 1, 
                                        min = 0, 
                                        max = 50)

    ppm_photons = IntProperty( name = "Photons Per Iteration", 
                                        description = "Number of photons emitted per pass", 
                                        default = 500000, 
                                        min = 1000, 
                                        max = 99000000)

    ppm_alpha = FloatProperty( name = "Photon Alpha", 
                                        description = "", 
                                        default = 0.666, 
                                        min = 0.01, 
                                        max = 1.0)
    
    ppm_initial_rad = FloatProperty( name = "Initial Radius", 
                                        description = "Photon initial radius", 
                                        default = 2.0, 
                                        min = 0.001, 
                                        max = 200)
    
    # Bidirectional MIS
    bidir_mis = BoolProperty( name = "Multiple Importance Sampling", 
                                        description = "Use multiple importance sampling for bidirectional path tracing", 
                                        default = True)

    vcm_mode = EnumProperty( items = [
                                        ('4', 'Bidirectional', 'Use bidirectional path tracing'), 
                                        ('5', 'VCM', 'Use vertex connection and merging')], 
                                        name = "Mode", 
                                        description = "", 
                                        default = '4')

    renderstamp_use = BoolProperty( name = "Use Render Stamp", 
                                        description =  "Use render stamp on final image -- Passes | Rays | Time", 
                                        default = False)

    vfb_type = EnumProperty( items = [('0', 'None', 'Use no-GUI unattended rendering'), ('2', 'Corona wx Framebuffer', 'Use Corona Standalone framebuffer GUI')],
                                        name = "Framebuffer", 
                                        description = "Use virtual frame buffer or use no-GUI unattended rendering", 
                                        default = '2')
                       
    export_path = StringProperty( name = "Export path", 
                                        description = "Where to export project files", 
                                        subtype = 'DIR_PATH')

def register():
    bpy.utils.register_class( CoronaRenderSettings)
    bpy.types.Scene.corona = PointerProperty( type  =  CoronaRenderSettings)
def unregister():
    bpy.utils.unregister_class( CoronaRenderSettings)
