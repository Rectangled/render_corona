import bpy
from bpy.props import FloatProperty, EnumProperty, BoolProperty, PointerProperty
import math

def update_cam_type( self, context):
    cam_data = context.object.data
    if cam_data.corona.camera_type == 'ortho':
        cam_data.type = 'ORTHO'
    if cam_data.corona.camera_type == 'perspective':
        cam_data.type = 'PERSP'

def update_cam_width( self, context):
    cam_data = context.object.data
    cam_data.ortho_scale = 2 * cam_data.corona.ortho_width

#------------------------------------
# Camera properties
#------------------------------------
class CoronaCameraProps( bpy.types.PropertyGroup):

    camera_type = bpy.props.EnumProperty( items = [
                                    ('ortho', 'Orthographic Camera', ''),
                                    ('perspective', 'Perspective Camera', '')],
                                    name = "Camera Type",
                                    description = "Camera model",
                                    default = 'perspective',
                                    update = update_cam_type)

    use_dof = bpy.props.BoolProperty( name = "Enable Depth of Field", 
                                    description = "Enable depth of the field rendering", 
                                    default = False)
                                    
    camera_dof = bpy.props.FloatProperty( name = "F-stop", 
                                    description = "Perspective camera f-stop value", 
                                    default = 16, 
                                    min = 0.01, 
                                    max = 50, 
                                    step =3, 
                                    precision = 1)

    bokeh_blades = bpy.props.IntProperty( name = "Blades", 
                                    description = "Number of camera aperture blades, which sets the shape of out-of-focus highlights - must be 3 or higher for polygonal bokeh",
                                    default = 3,
                                    min = 3,
                                    soft_max = 128)

    bokeh_rotation = bpy.props.FloatProperty( name = "Rotation", 
                                    description = "Relative rotation of bokeh shape in degrees",
                                    min = -math.pi,
                                    max = math.pi,
                                    subtype = 'ANGLE',
                                    unit = 'ROTATION')
    
    ortho_width = bpy.props.FloatProperty( name = "Width", 
                                    description = "Width of the captured image", 
                                    default = 4.0, 
                                    min = 1.0, 
                                    update = update_cam_width)

    use_region = bpy.props.BoolProperty( name = "Region Render Override",
                                    description = "Use border render region to zoom into the image, without changing resolution or creating black borders",
                                    default = False)

    render_clipping = bpy.props.BoolProperty( name = "Render Clipping",
                                    description = "Render camera clipping distances. Disabling renders without clipping",
                                    default = False)

    use_bokeh_img = bpy.props.BoolProperty( name = "Use Bokeh Image",
                                    description = "Specify custom bokeh shape using a texture map. Area with white color defines the aperture shape. Non-black and white texture maps can be used to fake chromatic abberation effects",
                                    default = False)

    bokeh_img = bpy.props.StringProperty( name = "Bokeh Image",
                                    description = "Texture map to define bokeh shape",
                                    default = '',
                                    subtype = 'FILE_PATH')
    
def register():
    bpy.utils.register_class( CoronaCameraProps)
    bpy.types.Camera.corona = PointerProperty( type = CoronaCameraProps)
def unregister():
    bpy.utils.unregister_class( CoronaCameraProps)
