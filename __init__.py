#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Some code has been adapted or borrowed from various exporters, including those 
# for NOX, Mitsuba, Luxrender, Sunflow, and appleseed. Many thanks to those developers
# for their ingenuity and elegant solutions.

bl_info = {
    "name": "Corona Render",
    "author": "Joel Daniels, elindell, Francesc Juhe (and some code borrowed from Franz Beaune and Esteban Tovagliari's appleseed exporter)",
    "version": (1, 9, 2),
    "blender": (2, 7, 2),
    "location": "Info Header (engine dropdown)",
    "description": "Corona Render integration",
    "warning": "Corona Renderer and this script are in alpha. Please contribute by reporting bugs to the issue tracker.",
    "wiki_url": "http://corona-renderer.com/wiki/blender2corona",
    "tracker_url": "https://bitbucket.org/ohsnapitsjoel/render_corona/issues",
    "category": "Render"}

if 'bpy' in locals():
    import imp
    imp.reload(ui)
    imp.reload(export)
    imp.reload(engine)
    imp.reload(util)
    imp.reload(properties)
    imp.reload(operators)
    imp.reload(preferences)

else:
    import bpy
    from . import ui
    from . import export
    from . import engine
    from . import util
    from . import properties
    from . import operators
    from . import preferences

#------------------------------------
# Register the script
#------------------------------------
def register():
    '''Register the module'''
    print( "--------------------------------------------")
    util.CrnUpdate( "Starting add-on, %s" % util.get_version_string())
    properties.register()
    ui.register()
    operators.register()
    preferences.register()
    bpy.utils.register_module( __name__)
    util.CrnUpdate( "Add-on started")
    print( "--------------------------------------------")

def unregister():
    '''Unregister the module'''
    print( "---------------------------------")
    util.CrnUpdate("Disabling add-on")
    properties.unregister()
    ui.unregister()
    operators.unregister()
    preferences.unregister()
    bpy.utils.unregister_module( __name__)
    util.CrnUpdate("Add-on disabled")
    print( "---------------------------------")

if __name__ == "__main__":
    register()
